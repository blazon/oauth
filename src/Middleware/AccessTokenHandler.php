<?php

declare(strict_types=1);

namespace Blazon\OAuth\Middleware;

use Closure;
use League\OAuth2\Server\AuthorizationServer;
use League\OAuth2\Server\Exception\OAuthServerException;
use Blazon\OAuth\Exception\OAuthHttpProblem;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

/** @SuppressWarnings(PHPMD.StaticAccess) */
class AccessTokenHandler implements RequestHandlerInterface
{
    protected AuthorizationServer $server;

    protected Closure $responseFactory;

    public function __construct(
        AuthorizationServer $server,
        callable $responseFactory
    ) {
        $this->server = $server;
        $this->responseFactory = $responseFactory;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        // Create a new response for the request
        $response = ($this->responseFactory)();

        try {
            return $this->server->respondToAccessTokenRequest($request, $response);
        } catch (OAuthServerException $exception) {
            throw OAuthHttpProblem::create($exception);
        }
    }
}
