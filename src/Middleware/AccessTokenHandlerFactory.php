<?php

declare(strict_types=1);

namespace Blazon\OAuth\Middleware;

use League\OAuth2\Server\AuthorizationServer;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;

class AccessTokenHandlerFactory
{
    public function __invoke(ContainerInterface $container): AccessTokenHandler
    {
        /** @var AuthorizationServer $server */
        $server = $container->get(AuthorizationServer::class);

        /** @var callable $responseFactory */
        $responseFactory = $container->get(ResponseInterface::class);

        return new AccessTokenHandler($server, $responseFactory);
    }
}
