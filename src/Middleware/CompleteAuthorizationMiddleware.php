<?php

declare(strict_types=1);

namespace Blazon\OAuth\Middleware;

use Blazon\OAuth\Exception\InvalidAttributeException;
use Closure;
use League\OAuth2\Server\AuthorizationServer;
use League\OAuth2\Server\RequestTypes\AuthorizationRequest;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class CompleteAuthorizationMiddleware implements RequestHandlerInterface
{
    protected AuthorizationServer $server;

    protected Closure $responseFactory;

    public function __construct(
        AuthorizationServer $server,
        callable $responseFactory
    ) {
        $this->server = $server;
        $this->responseFactory = $responseFactory;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $request = $request->getAttribute(AuthorizationRequest::class);

        if (!$request) {
            throw new InvalidAttributeException("Unable to locate Authorization in request.");
        }

        // Create a new response for the request
        /** @var ResponseInterface $response */
        $response = ($this->responseFactory)();

        return $this->server->completeAuthorizationRequest($request, $response);
    }
}
