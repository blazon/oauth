<?php

declare(strict_types=1);

namespace Blazon\OAuth\Middleware;

use League\OAuth2\Server\ResourceServer;
use Psr\Container\ContainerInterface;

class AuthenticationFactory
{
    public function __invoke(ContainerInterface $container): Authentication
    {
        /** @var ResourceServer $server */
        $server = $container->get(ResourceServer::class);

        return new Authentication($server);
    }
}
