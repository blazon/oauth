<?php

declare(strict_types=1);

namespace Blazon\OAuth\Middleware;

use Closure;
use Blazon\OAuth\Exception\OAuthHttpProblem;
use League\OAuth2\Server\AuthorizationServer;
use League\OAuth2\Server\Exception\OAuthServerException;
use League\OAuth2\Server\RequestTypes\AuthorizationRequest;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class ValidateAuthorizationMiddleware implements MiddlewareInterface
{
    protected AuthorizationServer $server;

    protected Closure $responseFactory;

    public function __construct(
        AuthorizationServer $server,
        callable $responseFactory
    ) {
        $this->server = $server;
        $this->responseFactory = $responseFactory;
    }

    /** @SuppressWarnings(PHPMD.StaticAccess) */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $authRequest = $request->getAttribute(AuthorizationRequest::class);

        if ($authRequest) {
            return $handler->handle($request);
        }

        try {
            // Validate the HTTP request and return an AuthorizationRequest object.
            $authRequest = $this->server->validateAuthorizationRequest($request);
            $request = $request->withAttribute(AuthorizationRequest::class, $authRequest);

            return $handler->handle($request);
        } catch (OAuthServerException $exception) {
            throw OAuthHttpProblem::create($exception);
        }
    }
}
