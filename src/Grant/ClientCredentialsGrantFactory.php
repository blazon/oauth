<?php

declare(strict_types=1);

namespace Blazon\OAuth\Grant;

use League\OAuth2\Server\Grant\ClientCredentialsGrant;

class ClientCredentialsGrantFactory
{
    public function __invoke(): ClientCredentialsGrant
    {
        return new ClientCredentialsGrant();
    }
}
