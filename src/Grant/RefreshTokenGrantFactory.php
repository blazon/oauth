<?php

declare(strict_types=1);

namespace Blazon\OAuth\Grant;

use Doctrine\ORM\EntityManagerInterface;
use League\OAuth2\Server\Grant\RefreshTokenGrant;
use Blazon\OAuth\Config\Config;
use Blazon\OAuth\Entity\RefreshToken;
use Blazon\OAuth\Repository\RefreshTokenRepository;
use Psr\Container\ContainerInterface;

class RefreshTokenGrantFactory
{
    public function __invoke(ContainerInterface $container): RefreshTokenGrant
    {
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $container->get('Blazon\OAuth\Doctrine\EntityManager');

        /** @var RefreshTokenRepository $refreshTokenRepo */
        $refreshTokenRepo = $entityManager->getRepository(RefreshToken::class);

        /** @var Config $oauthConfig */
        $oauthConfig = $container->get(Config::class);

        $grant = new RefreshTokenGrant($refreshTokenRepo);

        $grant->setRefreshTokenTTL($oauthConfig->getRefreshTokenExpireInterval());

        return $grant;
    }
}
