<?php

declare(strict_types=1);

namespace Blazon\OAuth\Grant;

use Doctrine\ORM\EntityManagerInterface;
use League\OAuth2\Server\Grant\AuthCodeGrant;
use Blazon\OAuth\Config\Config;
use Blazon\OAuth\Entity\AuthCode;
use Blazon\OAuth\Entity\RefreshToken;
use Blazon\OAuth\Repository\AuthCodeRepository;
use Blazon\OAuth\Repository\RefreshTokenRepository;
use Psr\Container\ContainerInterface;

class AuthorizationCodeGrantFactory
{
    public function __invoke(ContainerInterface $container): AuthCodeGrant
    {
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $container->get('Blazon\OAuth\Doctrine\EntityManager');

        /** @var AuthCodeRepository $authCodeRepo */
        $authCodeRepo = $entityManager->getRepository(AuthCode::class);

        /** @var RefreshTokenRepository $refreshTokenRepo */
        $refreshTokenRepo = $entityManager->getRepository(RefreshToken::class);

        /** @var Config $oauthConfig */
        $oauthConfig = $container->get(Config::class);

        $grant = new AuthCodeGrant(
            $authCodeRepo,
            $refreshTokenRepo,
            $oauthConfig->getAuthCodeExpireInterval()
        );

        $grant->setRefreshTokenTTL($oauthConfig->getRefreshTokenExpireInterval());

        return $grant;
    }
}
