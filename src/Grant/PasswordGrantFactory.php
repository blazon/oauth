<?php

declare(strict_types=1);

namespace Blazon\OAuth\Grant;

use Blazon\OAuth\Entity\UserInterface;
use Doctrine\ORM\EntityManagerInterface;
use League\OAuth2\Server\Grant\PasswordGrant;
use Blazon\OAuth\Config\Config;
use Blazon\OAuth\Entity\RefreshToken;
use Blazon\OAuth\Repository\RefreshTokenRepository;
use League\OAuth2\Server\Repositories\UserRepositoryInterface;
use Psr\Container\ContainerInterface;

class PasswordGrantFactory
{
    public function __invoke(ContainerInterface $container): PasswordGrant
    {
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $container->get('Blazon\OAuth\Doctrine\EntityManager');

        /** @var UserRepositoryInterface $userRepo */
        $userRepo = $entityManager->getRepository(UserInterface::class);

        /** @var RefreshTokenRepository $refreshTokenRepo */
        $refreshTokenRepo = $entityManager->getRepository(RefreshToken::class);

        /** @var Config $oauthConfig */
        $oauthConfig = $container->get(Config::class);

        $grant = new PasswordGrant(
            $userRepo,
            $refreshTokenRepo
        );

        $grant->setRefreshTokenTTL($oauthConfig->getRefreshTokenExpireInterval());

        return $grant;
    }
}
