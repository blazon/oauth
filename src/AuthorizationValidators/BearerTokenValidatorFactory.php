<?php

declare(strict_types=1);

namespace Blazon\OAuth\AuthorizationValidators;

use Doctrine\ORM\EntityManager;
use League\OAuth2\Server\AuthorizationValidators\BearerTokenValidator;
use Blazon\OAuth\Config\Config;
use Blazon\OAuth\Entity\AccessToken;
use Blazon\OAuth\Repository\AccessTokenRepository;
use Psr\Container\ContainerInterface;

class BearerTokenValidatorFactory
{
    public function __invoke(ContainerInterface $container): BearerTokenValidator
    {
        /** @var EntityManager $entityManager */
        $entityManager = $container->get('Blazon\OAuth\Doctrine\EntityManager');

        /** @var AccessTokenRepository $accessTokenRepo */
        $accessTokenRepo = $entityManager->getRepository(AccessToken::class);

        /** @var Config $config */
        $config = $container->get(Config::class);

        $validator = new BearerTokenValidator($accessTokenRepo);
        $validator->setEncryptionKey($config->getPublicKeyPath());

        return $validator;
    }
}
