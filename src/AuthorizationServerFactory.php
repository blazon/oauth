<?php

declare(strict_types=1);

namespace Blazon\OAuth;

use Doctrine\ORM\EntityManager;
use League\OAuth2\Server\AuthorizationServer;
use Blazon\OAuth\Config\Config;
use Blazon\OAuth\Entity\AccessToken;
use Blazon\OAuth\Entity\Client;
use Blazon\OAuth\Entity\Scope;
use Blazon\OAuth\Repository\AccessTokenRepository;
use Blazon\OAuth\Repository\ClientRepository;
use Blazon\OAuth\Repository\ScopeRepository;
use Psr\Container\ContainerInterface;

class AuthorizationServerFactory
{
    public function __invoke(ContainerInterface $container): AuthorizationServer
    {
        /** @var EntityManager $entityManager */
        $entityManager = $container->get('Blazon\OAuth\Doctrine\EntityManager');

        /** @var ClientRepository $clientRepository */
        $clientRepository = $entityManager->getRepository(Client::class);

        /** @var AccessTokenRepository $accessTokenRepo */
        $accessTokenRepo = $entityManager->getRepository(AccessToken::class);

        /** @var ScopeRepository $scopeRepository */
        $scopeRepository = $entityManager->getRepository(Scope::class);

        /** @var Config $config */
        $config = $container->get(Config::class);

        $encryptionKey = $config->getEncryptionKeyPath();

        if (is_file($encryptionKey)) {
            $encryptionKey = include $encryptionKey;
        }

        $server = new AuthorizationServer(
            $clientRepository,
            $accessTokenRepo,
            $scopeRepository,
            $config->getPrivateKeyPath(),
            $encryptionKey
        );

        foreach ($config->getGrants() as $grantServiceName) {
            $grant = $container->get($grantServiceName);

            $server->enableGrantType(
                $grant,
                $config->getAccessTokenExpireInterval()
            );
        }

        return $server;
    }
}
