<?php

declare(strict_types=1);

namespace Blazon\OAuth;

use Doctrine\ORM\EntityManager;
use League\OAuth2\Server\AuthorizationValidators\BearerTokenValidator;
use League\OAuth2\Server\ResourceServer;
use Blazon\OAuth\Config\Config;
use Blazon\OAuth\Entity\AccessToken;
use Blazon\OAuth\Repository\AccessTokenRepository;
use Psr\Container\ContainerInterface;

class ResourceServerFactory
{
    public function __invoke(ContainerInterface $container): ResourceServer
    {
        /** @var EntityManager $entityManager */
        $entityManager = $container->get('Blazon\OAuth\Doctrine\EntityManager');

        /** @var AccessTokenRepository $accessTokenRepo */
        $accessTokenRepo = $entityManager->getRepository(AccessToken::class);

        /** @var Config $config */
        $config = $container->get(Config::class);

        $validator = $container->get(BearerTokenValidator::class);

        return new ResourceServer($accessTokenRepo, $config->getPublicKeyPath(), $validator);
    }
}
