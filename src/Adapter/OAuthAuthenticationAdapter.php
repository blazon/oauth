<?php

declare(strict_types=1);

namespace Blazon\OAuth\Adapter;

use Blazon\OAuth\Entity\AccessToken;
use Blazon\OAuth\Exception\AccessTokenNotFoundException;
use Blazon\OAuth\Repository\AccessTokenRepository;
use League\OAuth2\Server\Exception\OAuthServerException;
use League\OAuth2\Server\ResourceServer;
use Mezzio\Authentication\AuthenticationInterface;
use Mezzio\Authentication\UserInterface;
use Mezzio\ProblemDetails\ProblemDetailsResponseFactory;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class OAuthAuthenticationAdapter implements AuthenticationInterface
{
    protected ResourceServer $server;

    protected AccessTokenRepository $repository;

    protected ProblemDetailsResponseFactory $problemFactory;

    public function __construct(
        ResourceServer $server,
        AccessTokenRepository $repository,
        ProblemDetailsResponseFactory $problemFactory
    ) {
        $this->server = $server;
        $this->repository = $repository;
        $this->problemFactory = $problemFactory;
    }

    public function authenticate(ServerRequestInterface $request): ?UserInterface
    {
        try {
            $response = $this->server->validateAuthenticatedRequest($request);
        } catch (OAuthServerException) {
            return null;
        }

        $accessTokenId = $response->getAttribute('oauth_access_token_id');

        if (!$accessTokenId) {
            return null;
        }

        try {
            /** @var AccessToken|null $accessToken */
            $accessToken = $this->repository->findAccessTokenByToken($accessTokenId);
        } catch (AccessTokenNotFoundException) {
            $accessToken = null;
        }

        if (!$accessToken) {
            return null;
        }

        /** @var UserInterface|null $user */
        $user = $accessToken->getUser();

        return $user;
    }

    /** @SuppressWarnings(PHPMD.UnusedFormalParameter) */
    public function unauthorizedResponse(ServerRequestInterface $request): ResponseInterface
    {
        return $this->problemFactory->createResponse($request, 401, 'Unauthorized');
    }
}
