<?php

declare(strict_types=1);

namespace Blazon\OAuth\Adapter;

use Blazon\OAuth\Entity\AccessToken;
use Blazon\OAuth\Repository\AccessTokenRepository;
use Doctrine\ORM\EntityManager;
use League\OAuth2\Server\ResourceServer;
use Mezzio\ProblemDetails\ProblemDetailsResponseFactory;
use Psr\Container\ContainerInterface;

class OAuthAuthenticationAdapterFactory
{
    public function __invoke(ContainerInterface $container): OAuthAuthenticationAdapter
    {
        /** @var EntityManager $entityManager */
        $entityManager = $container->get('Blazon\OAuth\Doctrine\EntityManager');

        /** @var AccessTokenRepository $accessTokenRepo */
        $accessTokenRepo = $entityManager->getRepository(AccessToken::class);

        /** @var ResourceServer $server */
        $server = $container->get(ResourceServer::class);

        $problemFactory = $container->get(ProblemDetailsResponseFactory::class);

        return new OAuthAuthenticationAdapter(
            $server,
            $accessTokenRepo,
            $problemFactory
        );
    }
}
