<?php

namespace Blazon\OAuth\EventListener;

use Blazon\DatabaseCore\Event\FetchRepository;
use Doctrine\Common\EventSubscriber;
use Blazon\OAuth\Config\Config;

class OAuthEventSubscriber implements EventSubscriber
{
    protected Config $config;

    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    public function fetchRepository(FetchRepository $event)
    {
        $repository = $event->getRepository();

        if ($repository instanceof ConfigAwareInterface) {
            $repository->setConfig($this->config);
        }
    }

    public function getSubscribedEvents(): array
    {
        return [FetchRepository::EVENT_NAME];
    }
}
