<?php

namespace Blazon\OAuth\EventListener;

use Blazon\OAuth\Config\Config;

interface ConfigAwareInterface
{
    public function getConfig(): Config;
    public function setConfig(Config $config): void;
}
