<?php

namespace Blazon\OAuth\EventListener;

use Blazon\OAuth\Config\Config;
use Psr\Container\ContainerInterface;

class OAuthEventSubscriberFactory
{
    public function __invoke(ContainerInterface $container): OAuthEventSubscriber
    {
        return new OAuthEventSubscriber($container->get(Config::class));
    }
}
