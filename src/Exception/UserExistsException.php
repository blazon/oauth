<?php

declare(strict_types=1);

namespace Blazon\OAuth\Exception;

use InvalidArgumentException;

class UserExistsException extends InvalidArgumentException
{
}
