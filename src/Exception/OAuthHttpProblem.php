<?php

namespace Blazon\OAuth\Exception;

use League\OAuth2\Server\Exception\OAuthServerException;
use Mezzio\ProblemDetails\Exception\CommonProblemDetailsExceptionTrait;
use Mezzio\ProblemDetails\Exception\ProblemDetailsExceptionInterface;
use RuntimeException;

class OAuthHttpProblem extends RuntimeException implements ProblemDetailsExceptionInterface
{
    use CommonProblemDetailsExceptionTrait;

    public static function create(OAuthServerException $exception): self
    {
        $newException = new self($exception->getMessage());
        $newException->status = $exception->getHttpStatusCode();
        $newException->detail = $exception->getMessage();
        $newException->type = 'https://httpstatuses.com/' . $exception->getHttpStatusCode();
        $newException->title = $exception->getErrorType();

        return $newException;
    }
}
