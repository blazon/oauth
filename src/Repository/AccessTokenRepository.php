<?php

declare(strict_types=1);

namespace Blazon\OAuth\Repository;

use Blazon\OAuth\Entity\AccessTokenInterface;
use Blazon\OAuth\Entity\UserInterface;
use Blazon\OAuth\Exception\InvalidAccessTokenException;
use Blazon\OAuth\Exception\UserNotFoundException;
use Doctrine\ORM\EntityRepository;
use League\OAuth2\Server\Entities\AccessTokenEntityInterface;
use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Exception\UniqueTokenIdentifierConstraintViolationException;
use League\OAuth2\Server\Repositories\AccessTokenRepositoryInterface;
use Blazon\OAuth\Entity\AccessToken;
use Blazon\OAuth\Exception\AccessTokenNotFoundException;

/** @SuppressWarnings(PHPMD.StaticAccess) */
class AccessTokenRepository extends EntityRepository implements AccessTokenRepositoryInterface
{
    public function getNewToken(
        ClientEntityInterface $clientEntity,
        array $scopes,
        $userIdentifier = null
    ): AccessToken {
        $accessToken = new AccessToken();
        $accessToken->setClient($clientEntity);
        $accessToken->setScope($scopes);

        if (!$userIdentifier) {
            return $accessToken;
        }

        $userRepository = $this->_em->getRepository(UserInterface::class);

        /** @var UserInterface|null $user */
        $user = $userRepository->findOneBy(['username' => $userIdentifier]);

        if (!$user) {
            throw new UserNotFoundException(
                'Unable to locate user'
            );
        }

        $accessToken->setUser($user);

        return $accessToken;
    }

    public function persistNewAccessToken(AccessTokenEntityInterface $accessTokenEntity)
    {
        try {
            $existingToken = $this->findAccessTokenByToken(
                $accessTokenEntity->getIdentifier()
            );
        } catch (AccessTokenNotFoundException $e) {
            // This is an expected exception.  Ignore this exception.
        }

        if (!empty($existingToken)) {
            throw UniqueTokenIdentifierConstraintViolationException::create();
        }

        if (!$accessTokenEntity instanceof AccessTokenInterface) {
            throw new InvalidAccessTokenException(
                "Access tokens must implement " . AccessTokenInterface::class
            );
        }

        $this->_em->persist($accessTokenEntity);
        $this->_em->flush();
    }

    public function findAccessTokenByToken($token): ?AccessToken
    {
        /** @var AccessToken|null $accessToken */
        $accessToken = $this->findOneBy(['token' => $token]);

        if (!$accessToken) {
            return null;
        }

        return $accessToken;
    }

    public function revokeAccessToken($tokenId): void
    {
        $accessToken = $this->findAccessTokenByToken($tokenId);
        $accessToken->setRevoked(true);
    }

    public function isAccessTokenRevoked($tokenId): bool
    {
        $accessToken = $this->findAccessTokenByToken($tokenId);

        if (!$accessToken) {
            return true;
        }

        return $accessToken->isRevoked();
    }
}
