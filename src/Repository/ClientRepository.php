<?php

declare(strict_types=1);

namespace Blazon\OAuth\Repository;

use Blazon\OAuth\Entity\ClientInterface;
use Doctrine\ORM\EntityRepository;
use League\OAuth2\Server\Exception\OAuthServerException;
use Blazon\OAuth\Entity\Client;
use Blazon\OAuth\Entity\Scope;
use Blazon\OAuth\EventListener\ConfigAwareInterface;
use Blazon\OAuth\Exception\ClientExistsException;
use Blazon\OAuth\Exception\ClientNotFoundException;

class ClientRepository extends EntityRepository implements ClientRepositoryInterface, ConfigAwareInterface
{
    use ConfigTrait;

    /**
     * @SuppressWarnings(PHPMD.BooleanArgumentFlag)
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    public function getClientEntity(
        $clientIdentifier,
        $grantType = null,
        $clientSecret = null,
        $mustValidateSecret = true
    ) {
        try {
            $client = $this->findOneByName($clientIdentifier);
        } catch (ClientNotFoundException $e) {
            return null;
        }

        if (!$grantType) {
            return $client;
        }

        if (!$this->isGrantAllowed($client, $grantType)) {
            throw OAuthServerException::unsupportedGrantType();
        }

        if (!$mustValidateSecret) {
            return $client;
        }

        if ($this->isSecretValid($client, $clientSecret)) {
            return $client;
        }

        return null;
    }

    public function createNewClient(
        string $name,
        string $secret,
        string $redirectUrl,
        array $allowedGrants,
        array $allowedScopes
    ): void {
        $exists = null;

        /** @var ScopeRepository $scopeRepo */
        $scopeRepo = $this->_em->getRepository(Scope::class);

        try {
            $exists = $this->findOneByName($name);
        } catch (ClientNotFoundException $e) {
            // This is an expected exception.  Ignore this exception.
        }

        if ($exists) {
            throw new ClientExistsException(
                'A client by the name of "' . $name . '" already exists'
            );
        }

        $client = new Client();
        $client->setName($name);
        $client->setSecret($secret);
        $client->setRedirectUrl($redirectUrl);
        $client->setGrants($allowedGrants);

        foreach ($allowedScopes as $index => $allowedScope) {
            if (!$allowedScope instanceof Scope) {
                $allowedScopes[$index] = $scopeRepo->findOneByName($allowedScope);
            }
        }

        $client->setScopes($allowedScopes);
        $this->_em->persist($client);
        $this->_em->flush($client);
    }

    public function deleteOneByName($name): void
    {
        $client = $this->findOneByName($name);
        $this->_em->remove($client);
        $this->_em->flush();
    }

    public function findOneByName($name): ClientInterface
    {
        /** @var ClientInterface|null $client */
        $client = $this->findOneBy(['name' => $name]);

        if (!$client) {
            throw new ClientNotFoundException(
                'A client by the name of ' . $name . ' was not found'
            );
        }

        return $client;
    }

    public function validateClient($clientIdentifier, $clientSecret, $grantType): bool
    {
        /** @var ClientInterface|null $client */
        $client = $this->findOneByName($clientIdentifier);

        if (!$client) {
            return false;
        }

        if (!$this->isGrantAllowed($client, $grantType)) {
            return false;
        }

        if ($this->isSecretValid($client, $clientSecret)) {
            return true;
        }

        return false;
    }

    protected function isSecretValid(ClientInterface $client, string $clientSecret): bool
    {
        if (!password_verify($clientSecret, $client->getSecret())) {
            return false;
        }

        $config = $this->getConfig();

        $rehashPassword = password_needs_rehash(
            $client->getSecret(),
            $config->getPasswordHashAlgorithm(),
            $config->getPasswordHashOptions()
        );

        if (!$rehashPassword) {
            return true;
        }

        $client->setSecret(password_hash(
            $clientSecret,
            $config->getPasswordHashAlgorithm(),
            $config->getPasswordHashOptions()
        ));

        $this->_em->flush($client);

        return true;
    }

    protected function isGrantAllowed(ClientInterface $client, ?string $grantType): bool
    {
        $allowedGrants = $client->getGrants();

        if (!in_array($grantType, $allowedGrants)) {
            return false;
        }

        return true;
    }
}
