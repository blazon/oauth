<?php

declare(strict_types=1);

namespace Blazon\OAuth\Repository;

use Blazon\OAuth\Entity\ClientInterface;
use Blazon\OAuth\Entity\UserInterface;
use Blazon\OAuth\Exception\InvalidClientException;
use Doctrine\ORM\EntityRepository;
use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Entities\ScopeEntityInterface;
use League\OAuth2\Server\Exception\OAuthServerException;
use League\OAuth2\Server\Repositories\ScopeRepositoryInterface;
use Blazon\OAuth\Entity\Scope;
use Blazon\OAuth\Exception\ScopeExistsException;
use Blazon\OAuth\Exception\ScopeNotFoundException;

class ScopeRepository extends EntityRepository implements ScopeRepositoryInterface
{
    public function getScopeEntityByIdentifier($identifier)
    {
        return $this->findOneBy(['name' => $identifier]);
    }

    /** @SuppressWarnings(PHPMD.StaticAccess) */
    public function finalizeScopes(
        array $scopes,
        $grantType,
        ClientEntityInterface $clientEntity,
        $userIdentifier = null
    ): array {
        if (!$clientEntity instanceof ClientInterface) {
            throw new InvalidClientException(
                "Client entity must implement: " . ClientInterface::class
            );
        }

        $user = null;

        if ($userIdentifier) {
            $user = $this->getUser($userIdentifier);
        }

        /** @var Scope $scope */
        foreach ($scopes as $scope) {
            $this->validateClientScope($scope, $grantType, $clientEntity);

            if ($user) {
                $this->validateUserScope($scope, $grantType, $user);
            }
        }

        return $scopes;
    }

    public function getUser($userIdentifier): UserInterface
    {
        /** @var UserInterface|null $user */
        $user = $this->_em->getRepository(UserInterface::class)
            ->findOneBy(['username' => $userIdentifier]);

        if (!$user) {
            throw OAuthServerException::accessDenied(
                'User not found'
            );
        }

        return $user;
    }

    public function validateClientScope($scope, $grantType, $clientEntity): void
    {
        // Validate Client
        if (!in_array($scope, $clientEntity->getScopes())) {
            throw OAuthServerException::invalidScope($scope->getName());
        }

        if (!in_array($grantType, $clientEntity->getGrants())) {
            throw OAuthServerException::invalidGrant($grantType);
        }
    }

    public function validateUserScope($scope, $grantType, $user): void
    {
        // Validate User
        if (!in_array($scope, $user->getScopes())) {
            throw OAuthServerException::invalidScope($scope->getName());
        }

        if (!in_array($grantType, $user->getGrants())) {
            throw OAuthServerException::invalidGrant($grantType);
        }
    }

    public function getAllScopeNames(): array
    {
        $scopes = $this->createQueryBuilder('a')
            ->select('a.name')
            ->getQuery()
            ->getScalarResult();

        if (empty($scopes)) {
            return [];
        }

        return array_column($scopes, 'name');
    }

    public function createScope($name): ScopeEntityInterface
    {
        $exists = null;

        try {
            $exists = $this->findOneByName($name);
        } catch (ScopeNotFoundException $e) {
            // This is an expected exception.  Ignore this exception.
        }

        if ($exists) {
            throw new ScopeExistsException(
                'A scope by the name of "' . $name . '"" already exists'
            );
        }

        $scope = new Scope();
        $scope->setName($name);

        $this->_em->persist($scope);
        $this->_em->flush($scope);

        return $scope;
    }

    public function deleteOneByName($name)
    {
        $scope = $this->findOneByName($name);
        $this->_em->remove($scope);
        $this->_em->flush();
    }

    public function findOneByName($name): ScopeEntityInterface
    {
        /** @var ScopeEntityInterface|null $scope */
        $scope = $this->findOneBy(['name' => $name]);

        if (!$scope) {
            throw new ScopeNotFoundException(
                'A scope by the name of ' . $name . ' was not found'
            );
        }

        return $scope;
    }
}
