<?php

declare(strict_types=1);

namespace Blazon\OAuth\Repository;

use Blazon\OAuth\Entity\AuthCodeInterface;
use League\OAuth2\Server\Repositories\AuthCodeRepositoryInterface as LeagueAuthCodeRepositoryInterface;

interface AuthCodeRepositoryInterface extends LeagueAuthCodeRepositoryInterface
{
    public function findOneByToken($token): AuthCodeInterface;
}
