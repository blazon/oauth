<?php

declare(strict_types=1);

namespace Blazon\OAuth\Repository;

use Blazon\OAuth\Entity\RefreshTokenInterface;
use League\OAuth2\Server\Repositories\RefreshTokenRepositoryInterface as LeagueRefreshTokenRepositoryInterface;

interface RefreshTokenRepositoryInterface extends LeagueRefreshTokenRepositoryInterface
{
    public function findOneByToken($token): RefreshTokenInterface;
}
