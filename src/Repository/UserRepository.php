<?php

declare(strict_types=1);

namespace Blazon\OAuth\Repository;

use Blazon\OAuth\Entity\ClientInterface;
use Blazon\OAuth\Entity\UserInterface;
use Blazon\OAuth\EventListener\ConfigAwareInterface;
use Blazon\OAuth\Exception\InvalidClientException;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Exception\OAuthServerException;
use League\OAuth2\Server\Repositories\UserRepositoryInterface;

/** @SuppressWarnings(PHPMD.StaticAccess) */
class UserRepository extends EntityRepository implements UserRepositoryInterface, ConfigAwareInterface
{
    use ConfigTrait;

    public function getUserEntityByUserCredentials(
        $username,
        $password,
        $grantType,
        ClientEntityInterface $clientEntity
    ): ?UserInterface {
        if (!$clientEntity instanceof ClientInterface) {
            throw new InvalidClientException(
                "Client must be an instance of: " . ClientInterface::class
            );
        }

        $user = $this->findOneByUserName($username);

        if (!$this->isPasswordValid($user, $password)) {
            throw OAuthServerException::invalidCredentials();
        }

        if (!$user->hasClient($clientEntity)) {
            throw OAuthServerException::invalidCredentials();
        }

        if (!$this->isGrantValid($user, $grantType)) {
            throw OAuthServerException::invalidCredentials();
        }

        return $user;
    }

    public function findOneByUserName(string $username): ?UserInterface
    {
        /** @var UserInterface|null $user */
        $user = $this->findOneBy(['username' => $username]);
        return $user;
    }

    public static function getClientIdSearchCriteria(ClientInterface $client): Criteria
    {
        return Criteria::create()
            ->where(Criteria::expr()->eq('id', $client->getId()));
    }

    protected function isGrantValid(UserInterface $user, string $grant): bool
    {
        $userGrants = $user->getGrants();
        return in_array($grant, $userGrants);
    }

    protected function isPasswordValid(UserInterface $user, string $password): bool
    {
        if (!password_verify($password, $user->getPassword())) {
            return false;
        }

        $config = $this->getConfig();

        $rehashPassword = password_needs_rehash(
            $password,
            $config->getPasswordHashAlgorithm(),
            $config->getPasswordHashOptions()
        );

        if (!$rehashPassword) {
            return true;
        }

        $user->setPassword(password_hash(
            $password,
            $config->getPasswordHashAlgorithm(),
            $config->getPasswordHashOptions()
        ));

        $this->_em->flush($user);

        return true;
    }
}
