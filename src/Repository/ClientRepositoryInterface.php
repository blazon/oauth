<?php

declare(strict_types=1);

namespace Blazon\OAuth\Repository;

use Blazon\OAuth\Entity\ClientInterface;
use League\OAuth2\Server\Repositories\ClientRepositoryInterface as LeagueClientRepositoryInterface;

interface ClientRepositoryInterface extends LeagueClientRepositoryInterface
{
    public function createNewClient(
        string $name,
        string $secret,
        string $redirectUrl,
        array $allowedGrants,
        array $allowedScopes
    ): void;

    public function deleteOneByName(string $name): void;
    public function findOneByName(string $name): ClientInterface;
}
