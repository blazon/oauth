<?php

declare(strict_types=1);

namespace Blazon\OAuth\Repository;

use Blazon\OAuth\Entity\RefreshTokenInterface;
use Blazon\OAuth\Exception\InvalidRefreshTokenException;
use Doctrine\ORM\EntityRepository;
use League\OAuth2\Server\Entities\RefreshTokenEntityInterface;
use League\OAuth2\Server\Exception\UniqueTokenIdentifierConstraintViolationException;
use Blazon\OAuth\Entity\RefreshToken;
use Blazon\OAuth\Exception\RefreshTokenNotFoundException;

class RefreshTokenRepository extends EntityRepository implements RefreshTokenRepositoryInterface
{
    public function getNewRefreshToken(): RefreshToken
    {
        return new RefreshToken();
    }

    /** @SuppressWarnings(PHPMD.StaticAccess) */
    public function persistNewRefreshToken(RefreshTokenEntityInterface $refreshTokenEntity)
    {
        if (!$refreshTokenEntity instanceof RefreshTokenInterface) {
            throw new InvalidRefreshTokenException(
                "Refresh token must implement: " . RefreshTokenInterface::class
            );
        }

        $exists = null;

        try {
            $exists = $this->findOneByToken($refreshTokenEntity->getToken());
        } catch (RefreshTokenNotFoundException $e) {
            // This is expected and ok.  Ignore this exception
        }

        if ($exists) {
            throw UniqueTokenIdentifierConstraintViolationException::create();
        }

        $this->_em->persist($refreshTokenEntity);
        $this->_em->flush($refreshTokenEntity);
    }

    public function revokeRefreshToken($tokenId)
    {
        $token = $this->findOneByToken($tokenId);
        $token->setRevoked(true);
        $this->_em->flush($token);
    }

    public function isRefreshTokenRevoked($tokenId): bool
    {
        $token = $this->findOneByToken($tokenId);
        return $token->isRevoked();
    }

    public function findOneByToken($token): RefreshTokenInterface
    {
        /** @var RefreshTokenInterface|null $refreshToken */
        $refreshToken = $this->findOneBy(['token' => $token]);

        if (!$refreshToken) {
            throw new RefreshTokenNotFoundException(
                'A refresh token by the token id of ' . $token . ' was not found'
            );
        }

        return $refreshToken;
    }
}
