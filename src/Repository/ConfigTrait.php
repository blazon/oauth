<?php

declare(strict_types=1);

namespace Blazon\OAuth\Repository;

use Blazon\OAuth\Config\Config;

trait ConfigTrait
{
    protected Config $config;

    public function getConfig(): Config
    {
        return $this->config;
    }

    public function setConfig(Config $config): void
    {
        $this->config = $config;
    }
}
