<?php

declare(strict_types=1);

namespace Blazon\OAuth\Repository;

use Blazon\OAuth\Entity\ClientInterface;
use Blazon\OAuth\Entity\UserInterface;
use Doctrine\Common\Collections\Criteria;
use League\OAuth2\Server\Repositories\UserRepositoryInterface as LeagueUserRepositoryInterface;

interface UserRepositoryInterface extends LeagueUserRepositoryInterface
{
    public static function getClientIdSearchCriteria(ClientInterface $client): Criteria;
    public function findOneByUserName(string $username): ?UserInterface;
}
