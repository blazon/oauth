<?php

declare(strict_types=1);

namespace Blazon\OAuth\Repository;

use Blazon\OAuth\Entity\AuthCodeInterface;
use Blazon\OAuth\Entity\Client;
use Blazon\OAuth\Entity\Scope;
use Blazon\OAuth\Entity\UserInterface;
use Blazon\OAuth\Exception\InvalidAuthCodeEntityException;
use Doctrine\ORM\EntityRepository;
use League\OAuth2\Server\Entities\AuthCodeEntityInterface;
use League\OAuth2\Server\Exception\UniqueTokenIdentifierConstraintViolationException;
use Blazon\OAuth\Entity\AuthCode;
use Blazon\OAuth\Exception\AuthCodeNotFoundException;

class AuthCodeRepository extends EntityRepository implements AuthCodeRepositoryInterface
{
    public function getNewAuthCode(): AuthCode
    {
        return new AuthCode();
    }

    /** @SuppressWarnings(PHPMD.StaticAccess) */
    public function persistNewAuthCode(AuthCodeEntityInterface $authCodeEntity)
    {
        $exists = null;

        if (!$authCodeEntity instanceof AuthCodeInterface) {
            throw new InvalidAuthCodeEntityException(
                "This repository requires an entity that implements: " . AuthCodeInterface::class
            );
        }

        try {
            $exists = $this->findOneByToken($authCodeEntity->getToken());
        } catch (AuthCodeNotFoundException $e) {
            // This is an expected exception.  Ignore this exception.
        }

        if ($exists) {
            throw UniqueTokenIdentifierConstraintViolationException::create();
        }

        /** @var UserInterface $user */
        $user = $this->_em->getRepository(UserInterface::class)
            ->findOneBy(['username' => $authCodeEntity->getUserIdentifier()]);

        $authCodeEntity->setUser($user);

        // Client and Scopes do not wake up correctly.  Lets fix them.
        $client = $this->_em->getRepository(Client::class)
            ->find($authCodeEntity->getClient()->getIdentifier());

        $authCodeEntity->setClient($client);

        $scopes = $authCodeEntity->getScopes();

        $scopeNames = [];

        foreach ($scopes as $scope) {
            $scopeNames[] = $scope->getIdentifier();
        }

        $fixedScopes = $this->_em->getRepository(Scope::class)
            ->findBy(['name' => $scopeNames]);

        $authCodeEntity->setScopes($fixedScopes);

        $this->_em->persist($authCodeEntity);
        $this->_em->flush();
    }

    public function revokeAuthCode($codeId): void
    {
        $authCode = $this->findOneByToken($codeId);
        $authCode->setRevoked(true);
        $this->_em->flush($authCode);
    }

    public function isAuthCodeRevoked($codeId): bool
    {
        $authCode = $this->findOneByToken($codeId);
        return $authCode->isRevoked();
    }

    public function findOneByToken($token): AuthCodeInterface
    {
        /** @var AuthCodeInterface|null $authCode */
        $authCode = $this->findOneBy(['token' => $token]);

        if (!$authCode) {
            throw new AuthCodeNotFoundException(
                'A Auth Code by the token id of ' . $token . ' was not found'
            );
        }

        return $authCode;
    }
}
