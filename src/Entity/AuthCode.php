<?php

declare(strict_types=1);

namespace Blazon\OAuth\Entity;

use DateTime;
use DateTimeImmutable;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Knp\DoctrineBehaviors\Contract\Entity\TimestampableInterface;
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampableTrait;
use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Entities\ScopeEntityInterface;

/**
 * Auth Code
 *
 * @ORM\Table(
 *     name="OAuthAuthCode"
 * )
 *
 * @ORM\Entity(repositoryClass="Blazon\OAuth\Repository\AuthCodeRepository")
 */
class AuthCode implements AuthCodeInterface, TimestampableInterface
{
    use TimestampableTrait;

    /**
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected int $id;

    /**
     * @ORM\Column(name="token", type="text", nullable=false)
     */
    protected string $token;

    /**
     * @ORM\Column(name="redirect_url", type="string", length=2000, nullable=false)
     */
    protected string $redirectUrl;

    /**
     * @ORM\Column(name="expires", type="date", nullable=false)
     */
    protected DateTimeInterface $expires;

    /**
     * @ORM\Column(name="revoked", type="boolean", nullable=false, options={"default" : 0})
     */
    protected bool $revoked = false;

    /**
     * @ORM\ManyToOne(targetEntity="UserInterface")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="cascade")
     */
    protected UserInterface $user;

    /**
     * @ORM\ManyToOne(targetEntity="Client", inversedBy="authCodes")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id", onDelete="cascade")
     */
    protected ClientEntityInterface $client;

    /**
     * @var Scope[]|ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Scope", inversedBy="authCodes")
     * @ORM\JoinTable(name="AuthCodeScopes",
     *      joinColumns={@ORM\JoinColumn(name="auth_code_id", referencedColumnName="id", onDelete="cascade")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="scope_id", referencedColumnName="id", onDelete="cascade")}
     * )
     */
    protected $scopes;

    // Placeholder for oauth
    private $userIdentitifer;

    public function __construct()
    {
        $this->scopes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getToken(): string
    {
        return $this->token;
    }

    public function setToken(string $token): void
    {
        $this->token = $token;
    }

    public function getRedirectUrl(): string
    {
        return $this->redirectUrl;
    }

    public function setRedirectUrl(string $redirectUrl): void
    {
        $this->redirectUrl = $redirectUrl;
    }

    public function getExpires(): DateTimeInterface
    {
        return $this->expires;
    }

    public function setExpires(DateTimeInterface $expires): void
    {
        $this->expires = $expires;
    }

    public function isRevoked(): bool
    {
        return $this->revoked;
    }

    public function setRevoked(bool $revoked): void
    {
        $this->revoked = $revoked;
    }

    public function getUser(): UserInterface
    {
        return $this->user;
    }

    public function setUser(UserInterface $user): void
    {
        $this->user = $user;
    }

    public function getClient(): ClientEntityInterface
    {
        return $this->client;
    }

    public function setClient(ClientEntityInterface $client)
    {
        $this->client = $client;
    }

    public function getScopes(): array
    {
        return $this->scopes->toArray();
    }

    public function setScopes(array $scopes)
    {
        $this->scopes->clear();

        foreach ($scopes as $scope) {
            $this->addScope($scope);
        }
    }

    public function addScope(ScopeEntityInterface $scope)
    {
        $this->scopes->add($scope);
    }

    /*
     * mandatory methods for oauth below
     */
    public function getRedirectUri(): string
    {
        return $this->getRedirectUrl();
    }

    public function setRedirectUri($uri)
    {
        $this->setRedirectUrl($uri);
    }

    public function getIdentifier(): ?string
    {
        return $this->getToken();
    }

    public function setIdentifier($identifier)
    {
        $this->setToken($identifier);
    }

    /** @SuppressWarnings(PHPMD.StaticAccess) */
    public function getExpiryDateTime(): DateTimeImmutable
    {
        /** @var DateTimeImmutable|DateTime $dateTime */
        $dateTime = $this->getExpires();

        if ($dateTime instanceof DateTimeImmutable) {
            return $dateTime;
        }

        return DateTimeImmutable::createFromMutable($dateTime);
    }

    public function setExpiryDateTime(DateTimeInterface $dateTime)
    {
        $this->setExpires($dateTime);
    }

    public function setUserIdentifier($identifier)
    {
        $this->userIdentitifer = $identifier;
    }

    public function getUserIdentifier()
    {
        return $this->userIdentitifer;
    }
}
