<?php

declare(strict_types=1);

namespace Blazon\OAuth\Entity;

use League\OAuth2\Server\Entities\AccessTokenEntityInterface;

interface AccessTokenInterface extends AccessTokenEntityInterface
{
    public function getId(): ?int;
}
