<?php

declare(strict_types=1);

namespace Blazon\OAuth\Entity;

use League\OAuth2\Server\Entities\AuthCodeEntityInterface;

interface AuthCodeInterface extends AuthCodeEntityInterface
{
    public function setUser(UserInterface $user): void;
    public function isRevoked(): bool;
    public function setRevoked(bool $revoked): void;
    public function getToken(): string;
    public function setScopes(array $scopes);
}
