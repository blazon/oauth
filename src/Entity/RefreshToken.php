<?php

declare(strict_types=1);

namespace Blazon\OAuth\Entity;

use Blazon\OAuth\Exception\InvalidAccessTokenException;
use DateTime;
use DateTimeImmutable;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Contract\Entity\TimestampableInterface;
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampableTrait;
use League\OAuth2\Server\Entities\AccessTokenEntityInterface;

/**
 * Access Token
 *
 * @ORM\Table(
 *     name="OAuthRefreshToken"
 * )
 * @ORM\Entity(repositoryClass="Blazon\OAuth\Repository\RefreshTokenRepository")
 */
class RefreshToken implements RefreshTokenInterface, TimestampableInterface
{
    use TimestampableTrait;

    /**
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected int $id;

    /**
     * @ORM\Column(name="token", type="text", nullable=false)
     */
    protected string $token;

    /**
     * @ORM\Column(name="expires", type="date", nullable=false)
     */
    protected DateTimeInterface $expires;

    /**
     * @ORM\Column(name="revoked", type="boolean", nullable=false, options={"default" : 0})
     */
    protected bool $revoked = false;

    /**
     * @ORM\OneToOne(targetEntity="AccessToken", inversedBy="refreshToken")
     * @ORM\JoinColumn(name="access_token_id", referencedColumnName="id", onDelete="cascade")
     */
    protected AccessTokenInterface $accessToken;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getToken(): string
    {
        return $this->token;
    }

    public function setToken(string $token): void
    {
        $this->token = $token;
    }

    public function getExpires(): DateTimeInterface
    {
        return $this->expires;
    }

    public function setExpires(DateTimeInterface $expires): void
    {
        $this->expires = $expires;
    }

    public function isRevoked(): bool
    {
        return $this->revoked;
    }

    public function setRevoked(bool $revoked): void
    {
        $this->revoked = $revoked;
    }

    public function setAccessToken(AccessTokenEntityInterface $accessToken)
    {
        if (!$accessToken instanceof AccessTokenInterface) {
            throw new InvalidAccessTokenException(
                "Access tokens must implement " . AccessTokenInterface::class
            );
        }

        $this->accessToken = $accessToken;
    }

    public function getAccessToken()
    {
        return $this->accessToken;
    }

    /*
     * mandatory methods for oauth below
     */
    public function getIdentifier(): string
    {
        return $this->getToken();
    }

    public function setIdentifier($identifier)
    {
        $this->setToken($identifier);
    }

    /** @SuppressWarnings(PHPMD.StaticAccesss) */
    public function getExpiryDateTime(): DateTimeImmutable
    {
        /** @var DateTime|DateTimeImmutable $date */
        $date = $this->getExpires();

        if ($date instanceof DateTimeImmutable) {
            return $date;
        }

        return DateTimeImmutable::createFromMutable($date);
    }

    public function setExpiryDateTime(DateTimeInterface $dateTime)
    {
        $this->setExpires($dateTime);
    }
}
