<?php

declare(strict_types=1);

namespace Blazon\OAuth\Entity;

use League\OAuth2\Server\Entities\RefreshTokenEntityInterface;

interface RefreshTokenInterface extends RefreshTokenEntityInterface
{
    public function getToken(): string;
    public function setRevoked(bool $revoked): void;
    public function isRevoked(): bool;
}
