<?php

declare(strict_types=1);

namespace Blazon\OAuth\Entity;

use League\OAuth2\Server\Entities\ClientEntityInterface;

interface ClientInterface extends ClientEntityInterface
{
    public function getId(): ?int;
    public function getScopes(): array;
    public function getGrants(): array;
    public function getSecret(): ?string;
    public function setGrants(array $grants): void;
    public function setName(string $name): void;
    public function setRedirectUrl(string $redirectUrl): void;
    public function setSecret(string $secret): void;
    public function setScopes(array $scopes): void;
}
