<?php

declare(strict_types=1);

namespace Blazon\OAuth\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Knp\DoctrineBehaviors\Contract\Entity\TimestampableInterface;
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampableTrait;
use League\OAuth2\Server\Entities\ScopeEntityInterface;

/**
 * Scope
 *
 * @ORM\Table(
 *     name="OAuthScope"
 * )
 * @ORM\Entity(repositoryClass="Blazon\OAuth\Repository\ScopeRepository")
 */
class Scope implements ScopeEntityInterface, TimestampableInterface
{
    use TimestampableTrait;

    /**
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected int $id;

    /**
     * @ORM\Column(name="name", type="string", nullable=false, unique=true)
     */
    protected string $name;

    /**
     * @var Client[]|ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Client", mappedBy="scopes")
     */
    protected $clients;

    /**
     * @var UserInterface[]|ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="UserInterface")
     * @ORM\JoinTable(name="UsersScopes",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="cascade")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="scope_id", referencedColumnName="id", onDelete="cascade")}
     * )
     */
    protected $users;

    /**
     * @var AccessToken[]|ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="AccessToken", mappedBy="scopes")
     */
    protected $accessTokens;

    /**
     * @var AuthCode[]|ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="AuthCode", mappedBy="scopes")
     */
    protected $authCodes;

    public function __construct()
    {
        $this->clients = new ArrayCollection();
        $this->users = new ArrayCollection();
        $this->accessTokens = new ArrayCollection();
        $this->authCodes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getClients()
    {
        return $this->clients;
    }

    public function setClients(array $clients): void
    {
        $this->clients->clear();

        foreach ($clients as $client) {
            $this->addClient($client);
        }
    }

    public function addClient(Client $client)
    {
        $client->addScope($this);
        $this->clients->add($client);
    }

    /**
     * @return ArrayCollection|UserInterface[]
     */
    public function getUsers()
    {
        return $this->users;
    }

    public function setUsers(array $users): void
    {
        $this->users->clear();

        foreach ($users as $user) {
            $this->addUser($user);
        }
    }

    public function addUser(UserInterface $user)
    {
        $this->users->add($user);
    }

    public function getAccessTokens()
    {
        return $this->accessTokens;
    }

    public function setAccessTokens(array $accessTokens): void
    {
        $this->accessTokens->clear();

        foreach ($accessTokens as $accessToken) {
            $this->addAccessToken($accessToken);
        }
    }

    public function addAccessToken(AccessToken $accessToken)
    {
        $accessToken->addScope($this);
        $this->accessTokens->add($accessToken);
    }

    public function getAuthCodes()
    {
        return $this->authCodes;
    }

    public function setAuthCodes($authCodes): void
    {
        $this->authCodes->clear();

        foreach ($authCodes as $authCode) {
            $this->addAuthCode($authCode);
        }
    }

    public function addAuthCode(AuthCode $authCode)
    {
        $authCode->addScope($this);
        $this->authCodes->add($authCode);
    }

    /*
     * mandatory methods for oauth below
     */
    public function getIdentifier(): string
    {
        return $this->getName();
    }

    public function jsonSerialize(): string
    {
        return $this->name;
    }
}
