<?php

declare(strict_types=1);

namespace Blazon\OAuth\Entity;

use Blazon\OAuth\Repository\UserRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Knp\DoctrineBehaviors\Contract\Entity\TimestampableInterface;
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampableTrait;
use League\OAuth2\Server\Entities\ScopeEntityInterface;

/**
 * Access Token
 *
 * @ORM\Table(
 *     name="OAuthUser"
 * )
 * @ORM\Entity(repositoryClass="Blazon\OAuth\Repository\ClientRepository")
 */
class User implements UserInterface, TimestampableInterface
{
    use TimestampableTrait;

    /**
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected int $id;

    /**
     * @ORM\Column(name="username", type="string", nullable=false, unique=true)
     */
    protected string $username;

    /**
     * @ORM\Column(name="password", type="string", nullable=false)
     */
    protected string $password;

    /**
     * @ORM\Column(name="grants", type="string", nullable=false)
     */
    protected string $grants;

    /**
     * @var ScopeEntityInterface[]|ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Scope")
     * @ORM\JoinTable(name="OAuthUserScopes",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="cascade")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="scope_id", referencedColumnName="id", onDelete="cascade")}
     * )
     */
    protected $scopes;

    /**
     * @var ClientInterface[]|ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Client")
     * @ORM\JoinTable(name="OAuthUserClients",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="cascade")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="client_id", referencedColumnName="id", onDelete="cascade")}
     * )
     */
    protected $clients;

    /**
     * @var RoleInterface[]|ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="RoleInterface")
     * @ORM\JoinTable(name="OAuthUserRoles",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="cascade")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="role_id", referencedColumnName="id", onDelete="cascade")}
     * )
     */
    protected $roles;

    public function __construct()
    {
        $this->scopes = new ArrayCollection();
        $this->clients = new ArrayCollection();
        $this->roles = new ArrayCollection();
    }

    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'username' => $this->getUsername(),
            'grants' => $this->getGrants()
        ];
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function setUsername(string $username): void
    {
        $this->username = $username;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    public function getGrants(): array
    {
        return explode(',', $this->grants);
    }

    public function setGrants(array $grants): void
    {
        $this->grants = implode(',', $grants);
    }

    public function getScopes(): array
    {
        return $this->scopes->toArray();
    }

    public function setScopes(array $scopes): void
    {
        $this->scopes->clear();

        foreach ($scopes as $scope) {
            $this->addScope($scope);
        }
    }

    public function addScope(ScopeEntityInterface $scope): void
    {
        $this->scopes->add($scope);
    }

    public function getClients(): array
    {
        return $this->clients->toArray();
    }

    public function setClients(array $clients): void
    {
        $this->clients->clear();

        foreach ($clients as $client) {
            $this->addClient($client);
        }
    }

    public function addClient(ClientInterface $client): void
    {
        $this->clients->add($client);
    }

    /** @SuppressWarnings(PHPMD.StaticAccess) */
    public function hasClient(ClientInterface $client): bool
    {
        $criteria = UserRepository::getClientIdSearchCriteria($client);

        $matching = $this->clients->matching($criteria);

        if ($matching->isEmpty()) {
            return false;
        }

        return true;
    }

    /*
     * mandatory methods for oauth below
     */
    public function getIdentifier(): string
    {
        return (string) $this->getUsername();
    }

    // Mezzio interface
    public function getIdentity(): string
    {
        return (string) $this->getUsername();
    }

    public function getRoles(): iterable
    {
        return $this->roles->toArray();
    }

    public function setRoles(iterable $roles)
    {
        $this->roles->clear();

        foreach ($roles as $role) {
            $this->addRole($role);
        }
    }

    public function addRole(RoleInterface $role)
    {
        $this->roles->add($role);
    }

    public function getDetail(string $name, $default = null)
    {
        $array = $this->toArray();
        return $array[$name] ?? $default;
    }

    public function getDetails(): array
    {
        return $this->toArray();
    }
}
