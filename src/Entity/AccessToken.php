<?php

declare(strict_types=1);

namespace Blazon\OAuth\Entity;

use DateTime;
use DateTimeImmutable;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Knp\DoctrineBehaviors\Contract\Entity\TimestampableInterface;
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampableTrait;
use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Entities\ScopeEntityInterface;
use League\OAuth2\Server\Entities\Traits\AccessTokenTrait;

/**
 * Access Token
 *
 * @ORM\Table(
 *     name="OAuthAccessToken"
 * )
 *
 * @ORM\Entity(repositoryClass="Blazon\OAuth\Repository\AccessTokenRepository")
 */
class AccessToken implements AccessTokenInterface, TimestampableInterface
{
    use AccessTokenTrait;
    use TimestampableTrait;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected int $id;

    /**
     * @ORM\Column(name="token", type="text", nullable=false)
     */
    protected string $token;

    /**
     * @ORM\Column(name="expires", type="date", nullable=false)
     */
    protected DateTimeInterface $expires;

    /**
     * @ORM\Column(name="revoked", type="boolean", nullable=false, options={"default" : 0})
     */
    protected bool $revoked = false;

    /**
     * @ORM\ManyToOne(targetEntity="UserInterface")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="cascade")
     */
    protected ?UserInterface $user = null;

    /**
     * @ORM\ManyToOne(targetEntity="Client", inversedBy="accessTokens")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id", onDelete="cascade")
     */
    protected ClientEntityInterface $client;

    /**
     * @var ScopeEntityInterface[]|ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Scope", inversedBy="accessTokens")
     * @ORM\JoinTable(name="AccessTokenScopes",
     *      joinColumns={@ORM\JoinColumn(name="access_token_id", referencedColumnName="id", onDelete="cascade")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="scope_id", referencedColumnName="id", onDelete="cascade")}
     * )
     */
    protected $scopes;

    /**
     * @ORM\OneToOne(targetEntity="RefreshToken", mappedBy="accessToken")
     */
    protected RefreshToken $refreshToken;

    public function __construct()
    {
        $this->scopes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getToken(): string
    {
        return $this->token;
    }

    public function setToken(string $token): void
    {
        $this->token = $token;
    }

    public function getExpires(): DateTimeInterface
    {
        return $this->expires;
    }

    public function setExpires(DateTimeInterface $expires): void
    {
        $this->expires = $expires;
    }

    public function isRevoked(): bool
    {
        return $this->revoked;
    }

    public function setRevoked(bool $revoked): void
    {
        $this->revoked = $revoked;
    }

    public function getUser(): ?UserInterface
    {
        return $this->user;
    }

    public function setUser(UserInterface $user): void
    {
        $this->user = $user;
    }

    /**
     * @return ScopeEntityInterface[]
     */
    public function getScopes(): array
    {
        return $this->scopes->toArray();
    }

    /**
     * @param ScopeEntityInterface[] $scopes
     */
    public function setScope(array $scopes): void
    {
        $this->scopes->clear();

        foreach ($scopes as $scope) {
            $this->addScope($scope);
        }
    }

    public function addScope(ScopeEntityInterface $scope)
    {
        $this->scopes->add($scope);
    }

    public function getRefreshToken(): RefreshToken
    {
        return $this->refreshToken;
    }

    public function setRefreshToken(RefreshToken $refreshToken): void
    {
        $refreshToken->setAccessToken($this);
        $this->refreshToken = $refreshToken;
    }

    /*
     * mandatory methods for oauth below
     */
    public function getIdentifier(): string
    {
        return $this->getToken();
    }

    public function setIdentifier($identifier): void
    {
        $this->setToken($identifier);
    }

    /** @SuppressWarnings(PHPMD.StaticAccesss) */
    public function getExpiryDateTime(): DateTimeImmutable
    {
        /** @var DateTime|DateTimeImmutable $date */
        $date = $this->getExpires();

        if ($date instanceof DateTimeImmutable) {
            return $date;
        }

        return DateTimeImmutable::createFromMutable($date);
    }

    public function setExpiryDateTime(DateTimeInterface $dateTime)
    {
        $this->setExpires($dateTime);
    }

    /**
     * Not used
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function setUserIdentifier($identifier): void
    {
    }

    /** @return mixed */
    public function getUserIdentifier()
    {
        $user = $this->getUser();

        if (!$user) {
            return null;
        }

        return $user->getIdentifier();
    }

    public function getClient(): ClientEntityInterface
    {
        return $this->client;
    }

    public function setClient(ClientEntityInterface $client): void
    {
        $this->client = $client;
    }
}
