<?php

declare(strict_types=1);

namespace Blazon\OAuth\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Knp\DoctrineBehaviors\Contract\Entity\TimestampableInterface;
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampableTrait;
use League\OAuth2\Server\Entities\ScopeEntityInterface;

/**
 * Access Token
 *
 * @ORM\Table(
 *     name="OAuthClient"
 * )
 * @ORM\Entity(repositoryClass="Blazon\OAuth\Repository\ClientRepository")
 */
class Client implements ClientInterface, TimestampableInterface
{
    use TimestampableTrait;

    /**
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected int $id;

    /**
     * @ORM\Column(name="name", type="string", nullable=false, unique=true)
     */
    protected string $name;

    /**
     * @ORM\Column(name="secret", type="string", nullable=false)
     */
    protected string $secret;

    /**
     * @ORM\Column(name="redirect_url", type="string", length=2000, nullable=false)
     */
    protected string $redirectUrl;

    /**
     * @ORM\Column(name="grants", type="string", nullable=false)
     */
    protected string $grants;

    /**
     * @ORM\Column(name="confidential", type="boolean", nullable=false, options={"default" : 0})
     */
    protected bool $confidential = false;

    /**
     * @var AuthCode[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AuthCode", mappedBy="client")
     */
    protected $authCodes;

    /**
     * @var AccessToken[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AccessToken", mappedBy="client")
     */
    protected $accessTokens;

    /**
     * @var ScopeEntityInterface[]|ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Scope", inversedBy="clients")
     * @ORM\JoinTable(name="ClientScopes",
     *      joinColumns={@ORM\JoinColumn(name="client_id", referencedColumnName="id", onDelete="cascade")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="scope_id", referencedColumnName="id", onDelete="cascade")}
     * )
     */
    protected $scopes;

    public function __construct()
    {
        $this->authCodes = new ArrayCollection();
        $this->accessTokens = new ArrayCollection();
        $this->scopes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getSecret(): ?string
    {
        return $this->secret;
    }

    public function setSecret(string $secret): void
    {
        $this->secret = $secret;
    }

    public function getRedirectUrl(): ?string
    {
        return $this->redirectUrl;
    }

    public function setRedirectUrl(string $redirectUrl): void
    {
        $this->redirectUrl = $redirectUrl;
    }

    public function getAuthCodes(): array
    {
        return $this->authCodes->toArray();
    }

    public function setAuthCodes(array $authCodes): void
    {
        $this->authCodes->clear();

        foreach ($authCodes as $authCode) {
            $this->addAuthCode($authCode);
        }
    }

    public function addAuthCode(AuthCode $authCode)
    {
        $authCode->setClient($this);
        $this->authCodes->add($authCode);
    }

    public function getAccessTokens(): array
    {
        return $this->accessTokens->toArray();
    }

    public function setAccessTokens(array $accessTokens): void
    {
        $this->accessTokens->clear();

        foreach ($accessTokens as $accessToken) {
            $this->addAccessToken($accessToken);
        }
    }

    public function addAccessToken(AccessToken $accessToken)
    {
        $accessToken->setClient($this);
        $this->accessTokens->add($accessToken);
    }

    public function getGrants(): array
    {
        return explode(',', $this->grants);
    }

    public function setGrants(array $grants): void
    {
        $this->grants = implode(',', $grants);
    }

    public function getScopes(): array
    {
        return $this->scopes->toArray();
    }

    public function setScopes(array $scopes): void
    {
        $this->scopes->clear();

        foreach ($scopes as $scope) {
            $this->addScope($scope);
        }
    }

    public function addScope(ScopeEntityInterface $scope): void
    {
        $this->scopes->add($scope);
    }

    /*
     * mandatory methods for oauth below
     */
    public function getIdentifier(): string
    {
        return (string) $this->getId();
    }

    public function getRedirectUri(): string
    {
        return $this->getRedirectUrl();
    }

    public function isConfidential(): bool
    {
        return $this->confidential;
    }

    public function setConfidential(bool $confidential)
    {
        $this->confidential = $confidential;
    }
}
