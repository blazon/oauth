<?php

declare(strict_types=1);

namespace Blazon\OAuth\Entity;

interface RoleInterface
{
    public function getId(): ?int;
    public function getName(): string;
    public function getParent(): ?RoleInterface;
    public function getChildren(): array;
    public function setParent(?RoleInterface $parent): void;
}
