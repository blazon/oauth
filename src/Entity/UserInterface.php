<?php

declare(strict_types=1);

namespace Blazon\OAuth\Entity;

use League\OAuth2\Server\Entities\UserEntityInterface;
use Mezzio\Authentication\UserInterface as MezzioUserInterface;

interface UserInterface extends UserEntityInterface, MezzioUserInterface
{
    public function getPassword(): string;
    public function setPassword(string $password): void;
    public function getScopes(): array;
    public function getGrants(): array;
    public function getClients(): array;
    public function hasClient(ClientInterface $client): bool;
}
