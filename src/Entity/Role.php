<?php

declare(strict_types=1);

namespace Blazon\OAuth\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Knp\DoctrineBehaviors\Contract\Entity\TimestampableInterface;
use Knp\DoctrineBehaviors\Model\Timestampable\TimestampableTrait;

/**
 * @ORM\Table(
 *     name="OAuthRole"
 * )
 * @ORM\Entity(repositoryClass="Blazon\OAuth\Repository\RoleRepository")
 */
class Role implements TimestampableInterface, RoleInterface
{
    use TimestampableTrait;

    /**
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected int $id;

    /**
     * @ORM\Column(name="name", type="string", nullable=false, unique=true)
     */
    protected string $name;

    /**
     * @var RoleInterface|null
     *
     * @ORM\ManyToOne(targetEntity="RoleInterface", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="cascade")
     */
    protected ?RoleInterface $parent;

    /**
     * @var RoleInterface[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="RoleInterface", mappedBy="parent")
     */
    protected $children;

    public function __construct()
    {
        $this->children = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getParent(): ?RoleInterface
    {
        return $this->parent;
    }

    public function setParent(?RoleInterface $parent): void
    {
        $this->parent = $parent;
    }

    public function getChildren(): array
    {
        return $this->children->toArray();
    }

    public function setChildren(iterable $children): void
    {
        $this->children->clear();

        foreach ($children as $child) {
            $this->addChild($child);
        }
    }

    public function addChild(RoleInterface $role): void
    {
        $role->setParent($this);
        $this->children->add($role);
    }
}
