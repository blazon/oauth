<?php

declare(strict_types=1);

namespace Blazon\OAuth\Command;

use Exception;
use RuntimeException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class KeyGenerator extends CommandAbstract
{
    protected function configure(): void
    {
        $this
            ->setName('oauth:generate-keys')
            ->setDescription('Generate encryption keys for Oauth2')
            ->addArgument(
                'privateKeyPath',
                InputArgument::OPTIONAL,
                'Path to install private key',
                $this->getConfig()->getPrivateKeyPath()
            )
            ->addArgument(
                'publicKeyPath',
                InputArgument::OPTIONAL,
                'Path to install public key',
                $this->getConfig()->getPublicKeyPath()
            )
            ->addArgument(
                'encryptionKeyPath',
                InputArgument::OPTIONAL,
                'Path to install encryption key',
                $this->getConfig()->getEncryptionKeyPath()
            );
    }

    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ): int {
        $privateKeyPath = $input->getArgument('privateKeyPath');

        if (!$this->isValidPath($privateKeyPath)) {
            throw new RuntimeException(
                'Unable to create private key. ' . $privateKeyPath
                . ' is either can\'t be written to or file already exists'
            );
        }

        $publicKeyPath = $input->getArgument('publicKeyPath');

        if (!$this->isValidPath($publicKeyPath)) {
            throw new RuntimeException(
                'Unable to create public key. ' . $publicKeyPath
                . ' is either can\'t be written to or file already exists'
            );
        }

        $encryptionKeyPath = $input->getArgument('encryptionKeyPath');

        if (!$this->isValidPath($encryptionKeyPath)) {
            throw new RuntimeException(
                'Unable to create encryption key. ' . $encryptionKeyPath
                . ' is either can\'t be written to or file already exists'
            );
        }

        $config = $this->getOpenSSLConfig();
        $privateKey = null;
        $key = openssl_pkey_new($config);
        openssl_pkey_export($key, $privateKey);

        $publicKey = openssl_pkey_get_details($key);

        try {
            $randomBytes = random_bytes(32);
        } catch (Exception $e) {
            throw new RuntimeException($e->getMessage());
        }

        $encryptionKey = base64_encode($randomBytes);

        file_put_contents($privateKeyPath, $privateKey);
        file_put_contents($publicKeyPath, $publicKey['key']);
        file_put_contents($encryptionKeyPath, sprintf("<?php\nreturn '%s';\n", $encryptionKey));

        chmod($privateKeyPath, 0600);
        chmod($publicKeyPath, 0600);
        chmod($encryptionKeyPath, 0600);

        $output->writeln('Keys generated to:');
        $output->writeln("\t Private Key: " . $privateKeyPath);
        $output->writeln("\t Public Key: " . $publicKeyPath);
        $output->writeln("\t Encryption Key: " . $encryptionKeyPath);

        return 0;
    }

    protected function getOpenSSLConfig(): array
    {
        return $this->getConfig()->getOpenSSLConfig();
    }

    protected function isValidPath($path): bool
    {
        if (
            empty($path)
            || !is_writable(dirname($path))
            || file_exists($path)
        ) {
            return false;
        }

        return true;
    }
}
