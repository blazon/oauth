<?php

declare(strict_types=1);

namespace Blazon\OAuth\Command\Client;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Secret extends AbstractClientCommand
{
    protected function configure(): void
    {
        $this
            ->setName('oauth:client:secret')
            ->setDescription('Change client secret')
            ->addArgument(
                'name',
                InputArgument::REQUIRED,
                'Client Id'
            );
    }

    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ): int {
        $client = $this->getClient($input);
        $secret = $this->getSecret($input, $output, 'client secret');
        $client->setSecret($secret);
        $this->getEntityManager()->flush();
        $output->writeln('Complete');

        return 0;
    }
}
