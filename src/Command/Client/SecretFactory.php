<?php

declare(strict_types=1);

namespace Blazon\OAuth\Command\Client;

use Psr\Container\ContainerInterface;

class SecretFactory
{
    public function __invoke(ContainerInterface $container): Secret
    {
        return new Secret($container);
    }
}
