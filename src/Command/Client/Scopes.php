<?php

declare(strict_types=1);

namespace Blazon\OAuth\Command\Client;

use Blazon\OAuth\Entity\ClientInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Scopes extends AbstractClientCommand
{
    protected function configure(): void
    {
        $this
            ->setName('oauth:client:scopes')
            ->setDescription('Add/Remove scopes to an existing client')
            ->addArgument(
                'name',
                InputArgument::REQUIRED,
                'Client Id'
            );
    }

    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ): int {
        /** @var ClientInterface $client */
        $client = $this->getClient($input);
        $allowedScopes = $this->getScopes($input, $output, $client);

        $scopes = [];

        foreach ($allowedScopes as $allowedScope) {
            $scopes[] = $this->getScopeRepository()->findOneByName($allowedScope);
        }

        $client->setScopes($scopes);
        $this->getEntityManager()->flush();
        $output->writeln('Complete');

        return 0;
    }
}
