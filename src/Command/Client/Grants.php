<?php

declare(strict_types=1);

namespace Blazon\OAuth\Command\Client;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Grants extends AbstractClientCommand
{
    protected function configure(): void
    {
        $this
            ->setName('oauth:client:grants')
            ->setDescription('Add/Remove grants to an existing client')
            ->addArgument(
                'name',
                InputArgument::REQUIRED,
                'Client Id'
            );
    }

    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ): int {
        $client = $this->getClient($input);
        $allowedGrants = $this->getGrants($input, $output, $client);
        $client->setGrants($allowedGrants);
        $this->getEntityManager()->flush();
        $output->writeln('Complete');
        return 0;
    }
}
