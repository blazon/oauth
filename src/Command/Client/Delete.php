<?php

declare(strict_types=1);

namespace Blazon\OAuth\Command\Client;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Delete extends AbstractClientCommand
{
    protected function configure(): void
    {
        $this
            ->setName('oauth:client:delete')
            ->setDescription('Delete existing client')
            ->addArgument(
                'name',
                InputArgument::REQUIRED,
                'Client Id'
            );
    }

    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ): int {
        $client = $this->getClient($input);
        $this->getEntityManager()->remove($client);
        $this->getEntityManager()->flush();
        $output->writeln('Complete');

        return 0;
    }
}
