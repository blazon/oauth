<?php

declare(strict_types=1);

namespace Blazon\OAuth\Command\Client;

use Blazon\OAuth\Exception\ClientExistsException;
use Blazon\OAuth\Exception\ClientNotFoundException;
use Blazon\OAuth\Exception\InvalidRedirectUrl;
use InvalidArgumentException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class Modify extends AbstractClientCommand
{
    protected function configure(): void
    {
        $this
            ->setName('oauth:client:modify')
            ->setDescription('Modify an existing clients name or redirect url')
            ->addArgument(
                'name',
                InputArgument::REQUIRED,
                'Client Id'
            )
            ->addOption(
                'name',
                null,
                InputOption::VALUE_OPTIONAL,
                'New Client Name'
            )
            ->addOption(
                'redirect',
                null,
                InputOption::VALUE_OPTIONAL,
                'New Client Redirect Url'
            );
    }

    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ): int {
        $client = $this->getClient($input);

        $newName = $input->getOption('name');
        $newRedirect = $input->getOption('redirect');

        if (!$newName && !$newRedirect) {
            throw new InvalidArgumentException(
                'Missing new name or redirect to update'
            );
        }

        if ($newName && $this->isNameValid($newName)) {
            $client->setName($newName);
            $output->writeln('<info>Name updated to: ' . $newName . '</info>');
        }

        if ($newRedirect) {
            $filteredRedirect = filter_var($newRedirect, FILTER_VALIDATE_URL);

            if (!$filteredRedirect) {
                $this->getEntityManager()->flush();
                throw new InvalidRedirectUrl($newRedirect . ' is invalid.');
            }

            $client->setRedirectUrl($filteredRedirect);
            $output->writeln('<info>Redirect URL updated to: ' . $filteredRedirect . '</info>');
        }

        $this->getEntityManager()->flush();
        $output->writeln('Complete');

        return 0;
    }

    public function isNameValid(string $newName): bool
    {
        try {
            $this->getClientRepository()->findOneByName($newName);
        } catch (ClientNotFoundException) {
            return true;
        }

        throw new ClientExistsException(
            'Client by the name of "' . $newName . '" already exists'
        );
    }
}
