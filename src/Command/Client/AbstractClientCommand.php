<?php

declare(strict_types=1);

namespace Blazon\OAuth\Command\Client;

use Blazon\OAuth\Command\CommandAbstract;
use Blazon\OAuth\Entity\Client;
use Blazon\OAuth\Entity\ClientInterface;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

abstract class AbstractClientCommand extends CommandAbstract
{
    public function getClient(InputInterface $input): ClientInterface
    {
        $name = $input->getArgument('name');
        return $this->getClientRepository()->findOneByName($name);
    }

    public function getRedirectUrl(InputInterface $input, OutputInterface $output): string
    {
        /** @var QuestionHelper $helper */
        $helper = $this->getHelper('question');

        $question = new Question('<question>Enter the clients redirect url:</question> ');

        $url = '';

        while (!$url) {
            $url = $helper->ask($input, $output, $question);
            $url = filter_var($url, FILTER_VALIDATE_URL);
        }

        return $url;
    }

    public function getGrants(
        InputInterface $input,
        OutputInterface $output,
        ?ClientInterface $currentClient = null
    ): array {
        $alreadySelected = [];
        $allGrants = array_keys($this->getConfig()->getGrants());

        if ($currentClient) {
            $alreadySelected = $currentClient->getGrants();
        }

        return $this->getChoices($input, $output, 'grants', $allGrants, $alreadySelected);
    }
}
