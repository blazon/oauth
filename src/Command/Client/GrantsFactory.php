<?php

declare(strict_types=1);

namespace Blazon\OAuth\Command\Client;

use Psr\Container\ContainerInterface;

class GrantsFactory
{
    public function __invoke(ContainerInterface $container): Grants
    {
        return new Grants($container);
    }
}
