<?php

declare(strict_types=1);

namespace Blazon\OAuth\Command\Client;

use Blazon\OAuth\Exception\ClientExistsException;
use Blazon\OAuth\Exception\ClientNotFoundException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Create extends AbstractClientCommand
{
    protected function configure(): void
    {
        $this
            ->setName('oauth:client:create')
            ->setDescription('Create a new client')
            ->addArgument(
                'name',
                InputArgument::REQUIRED,
                'Client name'
            );
    }

    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ): int {
        $name = $this->getClientName($input);
        $secret = $this->getSecret($input, $output, 'client secret');
        $redirectUrl = $this->getRedirectUrl($input, $output);
        $allowedGrants = $this->getGrants($input, $output);
        $allowedScopes = $this->getScopes($input, $output);

        $this->getClientRepository()->createNewClient(
            $name,
            $secret,
            $redirectUrl,
            $allowedGrants,
            $allowedScopes
        );

        $output->writeln('Client ' . $name . ' created successfully');

        return 0;
    }

    public function getClientName(InputInterface $input): string
    {
        $name = $input->getArgument('name');

        $exists = null;

        try {
            $exists = $this->getClientRepository()->findOneByName($name);
        } catch (ClientNotFoundException $e) {
            // This is ok for this method.  Just ignore this exception.
        }

        if ($exists) {
            throw new ClientExistsException(
                'A client by the name of "' . $name . '" already exists'
            );
        }

        return $name;
    }
}
