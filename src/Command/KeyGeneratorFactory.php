<?php

declare(strict_types=1);

namespace Blazon\OAuth\Command;

use Psr\Container\ContainerInterface;

class KeyGeneratorFactory
{
    public function __invoke(ContainerInterface $container): KeyGenerator
    {
        return new KeyGenerator($container);
    }
}
