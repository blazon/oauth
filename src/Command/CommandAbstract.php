<?php

declare(strict_types=1);

namespace Blazon\OAuth\Command;

use Blazon\OAuth\Config\Config;
use Blazon\OAuth\Entity\Client;
use Blazon\OAuth\Entity\Scope;
use Blazon\OAuth\Entity\UserInterface;
use Blazon\OAuth\Repository\ClientRepositoryInterface;
use Blazon\OAuth\Repository\ScopeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;
use RuntimeException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;

/** @SuppressWarnings(PHPMD.CouplingBetweenObjects) */
class CommandAbstract extends Command
{
    protected ContainerInterface $container;

    public function __construct(
        ContainerInterface $container,
        string $name = null
    ) {
        $this->container = $container;

        if (!$name) {
            parent::__construct();
            return;
        }

        parent::__construct($name);
    }

    protected function getSecret(
        InputInterface $input,
        OutputInterface $output,
        string $type
    ): string {
        /** @var QuestionHelper $helper */
        $helper = $this->getHelper('question');

        $question = new Question('<question>Enter new ' . $type . ':</question> ');
        $question->setHidden(true);
        $question->setHiddenFallback(false);

        $secret = false;
        $verify = false;

        while (!$secret) {
            $secret = $helper->ask($input, $output, $question);
        }

        $verifyQuestion = new Question('<question>Retype new ' . $type . ':</question> ');
        $verifyQuestion->setHidden(true);
        $verifyQuestion->setHiddenFallback(false);

        while (!$verify) {
            $verify = $helper->ask($input, $output, $verifyQuestion);
        }

        if ($secret !== $verify) {
            $output->writeln('<error>Secrets do not match.  Please try again.</error>');
            $secret = $this->getSecret($input, $output, $type);
        }

        return $this->hashString($secret);
    }

    protected function hashString($string): string
    {
        return password_hash(
            $string,
            $this->getConfig()->getPasswordHashAlgorithm(),
            $this->getConfig()->getPasswordHashOptions()
        );
    }

    protected function getScopes(
        InputInterface $input,
        OutputInterface $output,
        $current = null
    ): array {
        if (
            $current
            && !$current instanceof Client
            && !$current instanceof UserInterface
        ) {
            throw new RuntimeException(
                "Invalid current entity passed in"
            );
        }

        $alreadySelected = [];
        $allScopes = $this->getScopeRepository()->getAllScopeNames();

        if ($current) {
            $currentScopes = $current->getScopes();

            foreach ($currentScopes as $scope) {
                $alreadySelected[] = $scope->getIdentifier();
            }
        }

        return $this->getChoices($input, $output, 'scopes', $allScopes, $alreadySelected);
    }

    protected function getChoices(
        InputInterface $input,
        OutputInterface $output,
        string $type,
        array $allChoices,
        ?array $choicesSelected = []
    ): array {
        /** @var QuestionHelper $helper */
        $helper = $this->getHelper('question');

        sort($allChoices);

        while (true) {
            sort($choicesSelected);
            $displayGrants = array_combine(range(1, count($allChoices)), $allChoices);
            $displayGrants[] = 'Next';

            $questionText = '<info>Selected '
                . $type . ': ' . implode(', ', $choicesSelected) . "</info>\n"
                . '<question>Select allowed ' . $type . ' (select next to continue):</question> ';

            $grantsQuestion = new ChoiceQuestion(
                $questionText,
                $displayGrants,
                end($displayGrants)
            );

            $choice = $helper->ask($input, $output, $grantsQuestion);

            if ($choice === 'Next' && !empty($choicesSelected)) {
                break;
            } elseif ($choice === 'Next' && empty($choicesSelected)) {
                $output->writeln('<error>At least one ' . $type . ' must be provided</error>');
                continue;
            }

            if (in_array($choice, $choicesSelected)) {
                $choicesSelected = array_diff($choicesSelected, array($choice));
                continue;
            }

            $choicesSelected[] = $choice;
        }

        return $choicesSelected;
    }

    public function getContainer(): ContainerInterface
    {
        return $this->container;
    }

    public function getEntityManager(): EntityManagerInterface
    {
        return $this->getContainer()->get('Blazon\OAuth\Doctrine\EntityManager');
    }

    public function getScopeRepository(): ScopeRepository
    {
        /** @var ScopeRepository $repo */
        $repo = $this->getEntityManager()->getRepository(Scope::class);
        return $repo;
    }

    public function getClientRepository(): ClientRepositoryInterface
    {
        /** @var ClientRepositoryInterface $repo */
        $repo = $this->getEntityManager()->getRepository(Client::class);
        return $repo;
    }

    public function getConfig(): Config
    {
        return $this->getContainer()->get(Config::class);
    }
}
