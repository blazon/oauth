<?php

declare(strict_types=1);

namespace Blazon\OAuth\Command\Scope;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Create extends AbstractScopeCommand
{
    protected function configure(): void
    {
        $this
            ->setName('oauth:scope:create')
            ->setDescription('Create a new scope')
            ->addArgument(
                'name',
                InputArgument::REQUIRED,
                'Scope name'
            );
    }

    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ): int {
        $name = $input->getArgument('name');
        $this->getScopeRepository()->createScope($name);
        $output->writeln('Scope: ' . $name . ' created.');

        return 0;
    }
}
