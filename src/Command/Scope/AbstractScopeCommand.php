<?php

declare(strict_types=1);

namespace Blazon\OAuth\Command\Scope;

use Blazon\OAuth\Command\CommandAbstract;

class AbstractScopeCommand extends CommandAbstract
{
}
