<?php

declare(strict_types=1);

namespace Blazon\OAuth;

use Blazon\OAuth\Adapter\OAuthAuthenticationAdapter;
use Blazon\OAuth\Adapter\OAuthAuthenticationAdapterFactory;
use Blazon\OAuth\AuthorizationValidators\BearerTokenValidatorFactory;
use Blazon\OAuth\Command\Client\Grants;
use Blazon\OAuth\Command\Client\GrantsFactory;
use Blazon\OAuth\Command\Client\Modify;
use Blazon\OAuth\Command\Client\ModifyFactory;
use Blazon\OAuth\Command\Client\Scopes;
use Blazon\OAuth\Command\Client\ScopesFactory;
use Blazon\OAuth\Command\Client\Secret;
use Blazon\OAuth\Command\Client\SecretFactory;
use Blazon\OAuth\Command\KeyGenerator;
use Blazon\OAuth\Command\KeyGeneratorFactory;
use Blazon\OAuth\Command\Scope\Create;
use Blazon\OAuth\Command\Scope\CreateFactory;
use Blazon\OAuth\Command\Scope\Delete;
use Blazon\OAuth\Command\Scope\DeleteFactory;
use Blazon\OAuth\Config\Config;
use Blazon\OAuth\Config\ConfigFactory;
use Blazon\OAuth\Entity\Role;
use Blazon\OAuth\Entity\RoleInterface;
use Blazon\OAuth\Entity\User;
use Blazon\OAuth\Entity\UserInterface;
use Blazon\OAuth\EventListener\OAuthEventSubscriber;
use Blazon\OAuth\EventListener\OAuthEventSubscriberFactory;
use Blazon\OAuth\Grant\AuthorizationCodeGrantFactory;
use Blazon\OAuth\Grant\ClientCredentialsGrantFactory;
use Blazon\OAuth\Grant\ImplicitGrantFactory;
use Blazon\OAuth\Grant\PasswordGrantFactory;
use Blazon\OAuth\Grant\RefreshTokenGrantFactory;
use Blazon\OAuth\Middleware\AccessTokenHandler;
use Blazon\OAuth\Middleware\AccessTokenHandlerFactory;
use Blazon\OAuth\Middleware\Authentication;
use Blazon\OAuth\Middleware\AuthenticationFactory;
use Blazon\OAuth\Middleware\CompleteAuthorizationMiddleware;
use Blazon\OAuth\Middleware\CompleteAuthorizationMiddlewareFactory;
use Blazon\OAuth\Middleware\AuthTokenHandler;
use Blazon\OAuth\Middleware\AuthTokenHandlerFactory;
use Blazon\OAuth\Middleware\ValidateAuthorizationMiddleware;
use Blazon\OAuth\Middleware\ValidateAuthorizationMiddlewareFactory;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use League\OAuth2\Server\AuthorizationServer;
use League\OAuth2\Server\AuthorizationValidators\BearerTokenValidator;
use League\OAuth2\Server\Grant\AuthCodeGrant;
use League\OAuth2\Server\Grant\ClientCredentialsGrant;
use League\OAuth2\Server\Grant\ImplicitGrant;
use League\OAuth2\Server\Grant\PasswordGrant;
use League\OAuth2\Server\Grant\RefreshTokenGrant;
use League\OAuth2\Server\ResourceServer;

/** @SuppressWarnings(PHPMD.CouplingBetweenObjects) */
class ConfigProvider
{
    /** @SuppressWarnings(PHPMD.ExcessiveMethodLength) */
    public function __invoke(): array
    {
        return [
            'oauth2' => [
                'privateKeyPath'             => __DIR__ . '/../../../../data/private.key',
                'publicKeyPath'              => __DIR__ . '/../../../../data/public.key',
                'encryptionKeyPath'          => __DIR__ . '/../../../../data/encryption.key',
                'accessTokenExpireInterval'  => 'P1D',   // 1 day in DateInterval format
                'refreshTokenExpireInterval' => 'P1M',   // 1 month in DateInterval format
                'authCodeExpireInterval'     => 'PT10M', // 10 minutes in DateInterval format
                'authenticationRoute'        => 'auth',
                // Password Hash Params
                'passwordHash' => [
                    'algorithm' => PASSWORD_DEFAULT,
                    'options' => []
                ],

                'grants' => [
                    // Grants list should be [identifierName] => serviceName
                    'authorization_code' => 'authorization_code',
                    'client_credentials' => 'client_credentials',
                    'implicit'           => 'implicit',
                    'password'           => 'password',
                    'refresh_token'      => 'refresh_token',
                ],

                'openSSL' => [
                    'digest_alg' => 'sha1',
                    'private_key_bits' => 2048,
                    'private_key_type' => OPENSSL_KEYTYPE_RSA,
                ]
            ],

            'dependencies' => [
                'aliases' => [
                    'Blazon\OAuth\Doctrine\EntityManager' => 'doctrine.entity_manager.orm_default',
                    'authorization_code' => AuthCodeGrant::class,
                    'client_credentials' => ClientCredentialsGrant::class,
                    'implicit'           => ImplicitGrant::class,
                    'password'           => PasswordGrant::class,
                    'refresh_token'      => RefreshTokenGrant::class,
                ],
                'invokables' => [],
                'factories' => [
                    OAuthEventSubscriber::class => OAuthEventSubscriberFactory::class,
                    Config::class => ConfigFactory::class,
                    AuthorizationServer::class => AuthorizationServerFactory::class,
                    ResourceServer::class => ResourceServerFactory::class,
                    BearerTokenValidator::class => BearerTokenValidatorFactory::class,
                    OAuthAuthenticationAdapter::class => OAuthAuthenticationAdapterFactory::class,

                    /* Commands */
                    KeyGenerator::class => KeyGeneratorFactory::class,
                    Create::class => CreateFactory::class,
                    Delete::class => DeleteFactory::class,
                    Command\Client\Create::class => Command\Client\CreateFactory::class,
                    Modify::class => ModifyFactory::class,
                    Grants::class => GrantsFactory::class,
                    Scopes::class => ScopesFactory::class,
                    Secret::class => SecretFactory::class,
                    Command\Client\Delete::class => Command\Client\DeleteFactory::class,

                    /* Grants */
                    AuthCodeGrant::class => AuthorizationCodeGrantFactory::class,
                    ClientCredentialsGrant::class => ClientCredentialsGrantFactory::class,
                    ImplicitGrant::class => ImplicitGrantFactory::class,
                    PasswordGrant::class => PasswordGrantFactory::class,
                    RefreshTokenGrant::class => RefreshTokenGrantFactory::class,

                    /* Middleware */
                    Authentication::class => AuthenticationFactory::class,
                    CompleteAuthorizationMiddleware::class => CompleteAuthorizationMiddlewareFactory::class,
                    AuthTokenHandler::class => AuthTokenHandlerFactory::class,
                    ValidateAuthorizationMiddleware::class => ValidateAuthorizationMiddlewareFactory::class,
                    AccessTokenHandler::class => AccessTokenHandlerFactory::class,
                ],
            ],

            'doctrine' => [
                'driver' => [
                    'orm_default' => [
                        'drivers' => ['Blazon\OAuth\Entity' => 'oauth'],
                    ],
                    'oauth' => [
                        'class' => AnnotationDriver::class,
                        'cache' => 'array',
                        'paths' => __DIR__ . '/../src/Entity',
                    ],
                ],

                'event_manager' => [
                    'orm_default' => [
                        'subscribers' => [
                            OAuthEventSubscriber::class => OAuthEventSubscriber::class,
                        ],
                    ],
                ],

                'resolveTargetEntities' => [
                    UserInterface::class => User::class,
                    RoleInterface::class => Role::class,
                ]
            ],

            'console' => [
                'commands' => [
                    KeyGenerator::class => KeyGenerator::class,
                    Create::class => Create::class,
                    Delete::class => Delete::class,
                    Command\Client\Create::class => Command\Client\Create::class,
                    Modify::class => Modify::class,
                    Grants::class => Grants::class,
                    Scopes::class => Scopes::class,
                    Secret::class => Secret::class,
                    Command\Client\Delete::class => Command\Client\Delete::class,
                ],
            ],
        ];
    }
}
