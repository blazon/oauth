<?php

declare(strict_types=1);

namespace Blazon\OAuth\Test\AuthorizationValidators;

use Blazon\OAuth\AuthorizationValidators\BearerTokenValidatorFactory;
use Blazon\OAuth\Config\Config;
use Blazon\OAuth\Repository\AccessTokenRepository;
use Doctrine\ORM\EntityManagerInterface;
use League\OAuth2\Server\AuthorizationValidators\BearerTokenValidator;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;

/** @covers \Blazon\OAuth\AuthorizationValidators\BearerTokenValidatorFactory */
class BearerTokenValidatorFactoryTest extends TestCase
{
    public function testInvoke(): void
    {
        $mockContainer = $this->createMock(ContainerInterface::class);
        $mockEntityManager = $this->createMock(EntityManagerInterface::class);
        $mockAccessTokenRepo = $this->getMockBuilder(AccessTokenRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $mockConfig = $this->getMockBuilder(Config::class)
            ->disableOriginalConstructor()
            ->getMock();

        $map = [
            ['Blazon\OAuth\Doctrine\EntityManager', $mockEntityManager],
            [Config::class, $mockConfig],
        ];

        $mockContainer->expects($this->any())
            ->method('get')
            ->willReturnMap($map);

        $mockEntityManager->expects($this->once())
            ->method('getRepository')
            ->willReturn($mockAccessTokenRepo);

        $mockConfig->expects($this->once())
            ->method('getPublicKeyPath')
            ->willReturn(sys_get_temp_dir());

        $factory = new BearerTokenValidatorFactory();
        $result = $factory($mockContainer);

        $this->assertInstanceOf(BearerTokenValidator::class, $result);
    }
}
