<?php

declare(strict_types=1);

namespace Blazon\OAuth\Test\Command\Client;

use Blazon\OAuth\Command\Client\AbstractClientCommand;
use Blazon\OAuth\Config\Config;
use Blazon\OAuth\Entity\Client;
use Blazon\OAuth\Repository\ClientRepository;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

/** @covers \Blazon\OAuth\Command\Client\AbstractClientCommand */
class AbstractClientCommandTest extends TestCase
{
    /** @var AbstractClientCommand|MockObject  */
    protected $command;

    /** @var ClientRepository|MockObject */
    protected $mockClientRepo;

    /** @var Client|MockObject */
    protected $mockClient;

    /** @var Config|MockObject */
    protected $mockConfig;

    /** @var MockObject|InputInterface */
    protected $mockInput;

    /** @var MockObject|OutputInterface */
    protected $mockOutput;

    /** @var MockObject|ContainerInterface */
    protected $mockContainer;

    public function setUp(): void
    {
        $this->mockContainer = $this->createMock(ContainerInterface::class);

        $this->mockClientRepo = $this->getMockBuilder(ClientRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->mockClient = $this->getMockBuilder(Client::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->mockConfig = $this->getMockBuilder(Config::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->mockInput = $this->createMock(InputInterface::class);
        $this->mockOutput = $this->createMock(OutputInterface::class);

        $this->command = $this->getMockBuilder(AbstractClientCommand::class)
            ->setConstructorArgs([
                $this->mockContainer,
                'unit-tester'
            ])->onlyMethods(['getHelper', 'getChoices', 'getClientRepository', 'getConfig'])
            ->getMock();

        $this->assertInstanceOf(AbstractClientCommand::class, $this->command);
    }

    public function testConstructor(): void
    {
    }

    public function testGetClient(): void
    {
        $name = 'tester';
        $this->mockClient = $this->getMockBuilder(Client::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->mockInput->expects($this->once())
            ->method('getArgument')
            ->with($this->equalTo('name'))
            ->willReturn($name);

        $this->command->expects($this->once())
            ->method('getClientRepository')
            ->willReturn($this->mockClientRepo);

        $this->mockClientRepo->expects($this->once())
            ->method('findOneByName')
            ->with($this->equalTo($name))
            ->willReturn($this->mockClient);

        $result = $this->command->getClient($this->mockInput);
        $this->assertEquals($this->mockClient, $result);
    }

    public function testGetRedirectUrl(): void
    {
        $url = 'http://somewhere-to-go.com';

        $mockHelper = $this->getMockBuilder(QuestionHelper::class)
            ->disableOriginalConstructor()
            ->getMock();

        $mockHelper->expects($this->once())
            ->method('ask')
            ->with(
                $this->equalTo($this->mockInput),
                $this->equalTo($this->mockOutput),
                $this->isInstanceOf(Question::class)
            )->willReturn($url);

        $this->command->expects($this->once())
            ->method('getHelper')
            ->with($this->equalTo('question'))
            ->willReturn($mockHelper);

        $result = $this->command->getRedirectUrl($this->mockInput, $this->mockOutput);
        $this->assertEquals($url, $result);
    }

    public function testGetRedirectUrlNagsUserWhenBlank(): void
    {
        $url = 'http://somewhere-to-go.com';

        $counter = 0;

        $mockHelper = $this->getMockBuilder(QuestionHelper::class)
            ->disableOriginalConstructor()
            ->getMock();

        $mockHelper->expects($this->exactly(3))
            ->method('ask')
            ->with(
                $this->equalTo($this->mockInput),
                $this->equalTo($this->mockOutput),
                $this->isInstanceOf(Question::class)
            )->willReturnCallback(function () use (&$counter, $url) {
                $counter++;

                if ($counter < 3) {
                    return null;
                }

                return $url;
            });

        $this->command->expects($this->once())
            ->method('getHelper')
            ->with($this->equalTo('question'))
            ->willReturn($mockHelper);

        $result = $this->command->getRedirectUrl($this->mockInput, $this->mockOutput);
        $this->assertEquals($url, $result);
    }

    public function testGetGrants(): void
    {
        $grants = [
            'one' => 'some-grant-class',
            'two' => 'some-grant-class',
            'three' => 'some-grant-class'
        ];

        $alreadySelected = ['two'];

        $expected = ['one', 'three'];

        $this->command->expects($this->once())
            ->method('getConfig')
            ->willReturn($this->mockConfig);

        $this->mockConfig->expects($this->once())
            ->method('getGrants')
            ->willReturn($grants);

        $this->mockClient->expects($this->once())
            ->method('getGrants')
            ->willReturn($alreadySelected);

        $this->command->expects($this->once())
            ->method('getChoices')
            ->with(
                $this->equalTo($this->mockInput),
                $this->equalTo($this->mockOutput),
                'grants',
                $this->equalTo(array_keys($grants)),
                $this->equalTo($alreadySelected)
            )->willReturn($expected);

        $result = $this->command->getGrants(
            $this->mockInput,
            $this->mockOutput,
            $this->mockClient
        );

        $this->assertEquals($expected, $result);
    }

    public function testGetGrantsNoClient(): void
    {
        $grants = [
            'one' => 'some-grant-class',
            'two' => 'some-grant-class',
            'three' => 'some-grant-class'
        ];

        $expected = ['one', 'three'];

        $this->command->expects($this->once())
            ->method('getConfig')
            ->willReturn($this->mockConfig);

        $this->mockConfig->expects($this->once())
            ->method('getGrants')
            ->willReturn($grants);

        $this->mockClient->expects($this->never())
            ->method('getGrants');

        $this->command->expects($this->once())
            ->method('getChoices')
            ->with(
                $this->equalTo($this->mockInput),
                $this->equalTo($this->mockOutput),
                'grants',
                $this->equalTo(array_keys($grants)),
                $this->equalTo([])
            )->willReturn($expected);

        $result = $this->command->getGrants(
            $this->mockInput,
            $this->mockOutput
        );

        $this->assertEquals($expected, $result);
    }
}
