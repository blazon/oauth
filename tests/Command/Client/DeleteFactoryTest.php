<?php

declare(strict_types=1);

namespace Blazon\OAuth\Test\Command\Client;

use Blazon\OAuth\Command\Client\Delete;
use Blazon\OAuth\Command\Client\DeleteFactory;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;

/** @covers \Blazon\OAuth\Command\Client\DeleteFactory */
class DeleteFactoryTest extends TestCase
{
    public function testInvoke()
    {
        $container = $this->createMock(ContainerInterface::class);
        $factory = new DeleteFactory();

        $result = $factory($container);
        $this->assertInstanceOf(Delete::class, $result);
    }
}
