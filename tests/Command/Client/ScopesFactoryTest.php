<?php

declare(strict_types=1);

namespace Blazon\OAuth\Test\Command\Client;

use Blazon\OAuth\Command\Client\Scopes;
use Blazon\OAuth\Command\Client\ScopesFactory;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;

/** @covers \Blazon\OAuth\Command\Client\ScopesFactory */
class ScopesFactoryTest extends TestCase
{
    public function testInvoke()
    {
        $container = $this->createMock(ContainerInterface::class);
        $factory = new ScopesFactory();

        $result = $factory($container);
        $this->assertInstanceOf(Scopes::class, $result);
    }
}
