<?php

declare(strict_types=1);

namespace Blazon\OAuth\Test\Command\Client;

use Blazon\OAuth\Command\Client\Scopes;
use Blazon\OAuth\Entity\ClientInterface;
use Blazon\OAuth\Entity\Scope;
use Blazon\OAuth\Repository\ScopeRepository;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;
use ReflectionMethod;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/** @covers \Blazon\OAuth\Command\Client\Scopes */
class ScopesTest extends TestCase
{
    public function testConfigure()
    {
        $mockContainer = $this->createMock(ContainerInterface::class);
        $command = new Scopes($mockContainer);

        $name = $command->getName();
        $definition = $command->getDefinition();

        $this->assertEquals('oauth:client:scopes', $name);

        $arg = $definition->getArgument('name');
        $this->assertTrue($arg->isRequired());
    }

    public function testExecute()
    {
        $mockInput = $this->createMock(InputInterface::class);
        $mockOutput = $this->createMock(OutputInterface::class);
        $mockClient = $this->createMock(ClientInterface::class);
        $mockEm = $this->createMock(EntityManagerInterface::class);
        $scope = 'some-scope';
        $scopeEntity = $this->createMock(Scope::class);
        $mockScopeRepo = $this->createMock(ScopeRepository::class);

        $command = $this->getMockBuilder(Scopes::class)
            ->onlyMethods([
                'getClient',
                'getEntityManager',
                'getScopes',
                'getScopeRepository',
            ])->disableOriginalConstructor()
            ->getMock();

        $command->expects($this->once())
            ->method('getClient')
            ->with($this->equalTo($mockInput))
            ->willReturn($mockClient);

        $command->expects($this->any())
            ->method('getEntityManager')
            ->willReturn($mockEm);

        $command->expects($this->any())
            ->method('getScopes')
            ->willReturn([$scope]);

        $command->expects($this->once())
            ->method('getScopeRepository')
            ->willReturn($mockScopeRepo);

        $mockScopeRepo->expects($this->once())
            ->method('findOneByName')
            ->with($this->equalTo($scope))
            ->willReturn($scopeEntity);

        $mockClient->expects($this->once())
            ->method('setScopes')
            ->with($this->equalTo([$scopeEntity]));

        $mockOutput->expects($this->once())
            ->method('writeln')
            ->with($this->stringContains('Complete'));

        $mockEm->expects($this->once())
            ->method('flush');

        $method = new ReflectionMethod($command, 'execute');
        $method->setAccessible(true);

        $result = $method->invokeArgs($command, [$mockInput, $mockOutput]);

        $this->assertEquals(0, $result);
    }
}
