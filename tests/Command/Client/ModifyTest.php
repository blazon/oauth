<?php

declare(strict_types=1);

namespace Blazon\OAuth\Test\Command\Client;

use Blazon\OAuth\Command\Client\Grants;
use Blazon\OAuth\Command\Client\Modify;
use Blazon\OAuth\Entity\ClientInterface;
use Blazon\OAuth\Exception\ClientExistsException;
use Blazon\OAuth\Exception\ClientNotFoundException;
use Blazon\OAuth\Exception\InvalidRedirectUrl;
use Blazon\OAuth\Repository\ClientRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;
use ReflectionMethod;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @covers \Blazon\OAuth\Command\Client\Modify
 * @SuppressWarnings(PHPMD)
 */
class ModifyTest extends TestCase
{
    public function testConfigure()
    {
        $mockContainer = $this->createMock(ContainerInterface::class);

        $command = new Modify($mockContainer);

        $name = $command->getName();
        $definition = $command->getDefinition();

        $this->assertEquals('oauth:client:modify', $name);

        $arg = $definition->getArgument('name');
        $this->assertTrue($arg->isRequired());

        $option = $definition->getOption('name');
        $this->assertTrue($option->isValueOptional());

        $option = $definition->getOption('redirect');
        $this->assertTrue($option->isValueOptional());
    }

    public function testExecute()
    {
        $name = 'some-name';
        $redirect = 'http://somewhere.com';

        $mockInput = $this->createMock(InputInterface::class);
        $mockOutput = $this->createMock(OutputInterface::class);
        $mockClient = $this->createMock(ClientInterface::class);
        $mockEm = $this->createMock(EntityManagerInterface::class);

        $command = $this->getMockBuilder(Modify::class)
            ->onlyMethods([
                'getClient',
                'getEntityManager',
                'isNameValid'
            ])->disableOriginalConstructor()
            ->getMock();

        $command->expects($this->once())
            ->method('getClient')
            ->with($this->equalTo($mockInput))
            ->willReturn($mockClient);

        $inputMap = [
            ['name', $name],
            ['redirect', $redirect],
        ];

        $mockInput->expects($this->any())
            ->method('getOption')
            ->willReturnMap($inputMap);

        $command->expects($this->once())
            ->method('isNameValid')
            ->with($this->equalTo($name))
            ->willReturn(true);

        $mockClient->expects($this->once())
            ->method('setName')
            ->with($this->equalTo($name));

        $mockClient->expects($this->once())
            ->method('setRedirectUrl')
            ->with($this->equalTo($redirect));

        $command->expects($this->any())
            ->method('getEntityManager')
            ->willReturn($mockEm);

        $mockOutput->expects($this->atLeast(3))
            ->method('writeln')
            ->with($this->isType('string'));

        $mockEm->expects($this->once())
            ->method('flush');

        $method = new ReflectionMethod($command, 'execute');
        $method->setAccessible(true);

        $result = $method->invokeArgs($command, [$mockInput, $mockOutput]);

        $this->assertEquals(0, $result);
    }

    public function testExecuteInvalidRedirect()
    {
        $this->expectException(InvalidRedirectUrl::class);
        $name = 'some-name';
        $redirect = 'invalid uri';

        $mockInput = $this->createMock(InputInterface::class);
        $mockOutput = $this->createMock(OutputInterface::class);
        $mockClient = $this->createMock(ClientInterface::class);
        $mockEm = $this->createMock(EntityManagerInterface::class);

        $command = $this->getMockBuilder(Modify::class)
            ->onlyMethods([
                'getClient',
                'getEntityManager',
                'isNameValid'
            ])->disableOriginalConstructor()
            ->getMock();

        $command->expects($this->once())
            ->method('getClient')
            ->with($this->equalTo($mockInput))
            ->willReturn($mockClient);

        $inputMap = [
            ['name', $name],
            ['redirect', $redirect],
        ];

        $mockInput->expects($this->any())
            ->method('getOption')
            ->willReturnMap($inputMap);

        $command->expects($this->once())
            ->method('isNameValid')
            ->with($this->equalTo($name))
            ->willReturn(true);

        $mockClient->expects($this->once())
            ->method('setName')
            ->with($this->equalTo($name));

        $mockClient->expects($this->never())
            ->method('setRedirectUrl');

        $command->expects($this->any())
            ->method('getEntityManager')
            ->willReturn($mockEm);

        $mockOutput->expects($this->atLeast(1))
            ->method('writeln')
            ->with($this->isType('string'));

        $mockEm->expects($this->once())
            ->method('flush');

        $method = new ReflectionMethod($command, 'execute');
        $method->setAccessible(true);

        $method->invokeArgs($command, [$mockInput, $mockOutput]);
    }

    public function testExecuteMissingOptions()
    {
        $this->expectException(InvalidArgumentException::class);
        $name = null;
        $redirect = null;

        $mockInput = $this->createMock(InputInterface::class);
        $mockOutput = $this->createMock(OutputInterface::class);
        $mockClient = $this->createMock(ClientInterface::class);
        $mockEm = $this->createMock(EntityManagerInterface::class);

        $command = $this->getMockBuilder(Modify::class)
            ->onlyMethods([
                'getClient',
                'getEntityManager',
                'isNameValid'
            ])->disableOriginalConstructor()
            ->getMock();

        $command->expects($this->once())
            ->method('getClient')
            ->with($this->equalTo($mockInput))
            ->willReturn($mockClient);

        $inputMap = [
            ['name', $name],
            ['redirect', $redirect],
        ];

        $mockInput->expects($this->any())
            ->method('getOption')
            ->willReturnMap($inputMap);

        $command->expects($this->never())
            ->method('isNameValid');

        $mockClient->expects($this->never())
            ->method('setName');

        $mockClient->expects($this->never())
            ->method('setRedirectUrl');

        $command->expects($this->never())
            ->method('getEntityManager');

        $mockOutput->expects($this->never())
            ->method('writeln');

        $mockEm->expects($this->never())
            ->method('flush');

        $method = new ReflectionMethod($command, 'execute');
        $method->setAccessible(true);

        $method->invokeArgs($command, [$mockInput, $mockOutput]);
    }

    public function testIsNameValidTrue()
    {
        $name = 'some-name';

        $mockRepo = $this->createMock(ClientRepositoryInterface::class);
        $mockEntity = $this->createMock(ClientInterface::class);

        $command = $this->getMockBuilder(Modify::class)
            ->onlyMethods([
                'getClientRepository'
            ])->disableOriginalConstructor()
            ->getMock();

        $command->expects($this->once())
            ->method('getClientRepository')
            ->willReturn($mockRepo);

        $mockRepo->expects($this->once())
            ->method('findOneByName')
            ->with($this->equalTo($name))
            ->willThrowException(new ClientNotFoundException());

        $result = $command->isNameValid($name);
        $this->assertTrue($result);
    }

    public function testIsNameValidFalse()
    {
        $this->expectException(ClientExistsException::class);

        $name = 'some-name';

        $mockRepo = $this->createMock(ClientRepositoryInterface::class);
        $mockEntity = $this->createMock(ClientInterface::class);

        $command = $this->getMockBuilder(Modify::class)
            ->onlyMethods([
                              'getClientRepository'
                          ])->disableOriginalConstructor()
            ->getMock();

        $command->expects($this->once())
            ->method('getClientRepository')
            ->willReturn($mockRepo);

        $mockRepo->expects($this->once())
            ->method('findOneByName')
            ->with($this->equalTo($name))
            ->willReturn($mockEntity);

        $command->isNameValid($name);
    }
}
