<?php

declare(strict_types=1);

namespace Blazon\OAuth\Test\Command\Client;

use Blazon\OAuth\Command\Client\Secret;
use Blazon\OAuth\Command\Client\SecretFactory;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;

/** @covers \Blazon\OAuth\Command\Client\SecretFactory */
class SecretFactoryTest extends TestCase
{
    public function testInvoke()
    {
        $container = $this->createMock(ContainerInterface::class);
        $factory = new SecretFactory();

        $result = $factory($container);
        $this->assertInstanceOf(Secret::class, $result);
    }
}
