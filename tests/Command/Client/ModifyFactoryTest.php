<?php

declare(strict_types=1);

namespace Blazon\OAuth\Test\Command\Client;

use Blazon\OAuth\Command\Client\Modify;
use Blazon\OAuth\Command\Client\ModifyFactory;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;

/** @covers \Blazon\OAuth\Command\Client\ModifyFactory */
class ModifyFactoryTest extends TestCase
{
    public function testInvoke()
    {
        $container = $this->createMock(ContainerInterface::class);
        $factory = new ModifyFactory();

        $result = $factory($container);
        $this->assertInstanceOf(Modify::class, $result);
    }
}
