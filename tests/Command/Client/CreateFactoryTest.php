<?php

declare(strict_types=1);

namespace Blazon\OAuth\Test\Command\Client;

use Blazon\OAuth\Command\Client\Create;
use Blazon\OAuth\Command\Client\CreateFactory;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;

/** @covers \Blazon\OAuth\Command\Client\CreateFactory */
class CreateFactoryTest extends TestCase
{
    public function testInvoke()
    {
        $container = $this->createMock(ContainerInterface::class);
        $factory = new CreateFactory();

        $result = $factory($container);
        $this->assertInstanceOf(Create::class, $result);
    }
}
