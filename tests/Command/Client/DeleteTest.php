<?php

declare(strict_types=1);

namespace Blazon\OAuth\Test\Command\Client;

use Blazon\OAuth\Command\Client\Delete;
use Blazon\OAuth\Entity\ClientInterface;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;
use ReflectionMethod;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/** @covers \Blazon\OAuth\Command\Client\Delete */
class DeleteTest extends TestCase
{
    public function testConfigure()
    {
        $mockContainer = $this->createMock(ContainerInterface::class);
        $command = new Delete($mockContainer);

        $name = $command->getName();
        $definition = $command->getDefinition();

        $this->assertEquals('oauth:client:delete', $name);

        $arg = $definition->getArgument('name');
        $this->assertTrue($arg->isRequired());
    }

    public function testExecute()
    {
        $mockInput = $this->createMock(InputInterface::class);
        $mockOutput = $this->createMock(OutputInterface::class);
        $mockClient = $this->createMock(ClientInterface::class);
        $mockEm = $this->createMock(EntityManagerInterface::class);

        $command = $this->getMockBuilder(Delete::class)
            ->onlyMethods([
                'getClient',
                'getEntityManager'
            ])->disableOriginalConstructor()
            ->getMock();

        $command->expects($this->once())
            ->method('getClient')
            ->with($this->equalTo($mockInput))
            ->willReturn($mockClient);

        $command->expects($this->any())
            ->method('getEntityManager')
            ->willReturn($mockEm);

        $mockOutput->expects($this->once())
            ->method('writeln')
            ->with($this->stringContains('Complete'));

        $mockEm->expects($this->once())
            ->method('remove')
            ->with($this->equalTo($mockClient));

        $mockEm->expects($this->once())
            ->method('flush');

        $method = new ReflectionMethod($command, 'execute');
        $method->setAccessible(true);

        $result = $method->invokeArgs($command, [$mockInput, $mockOutput]);

        $this->assertEquals(0, $result);
    }
}
