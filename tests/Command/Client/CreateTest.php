<?php

declare(strict_types=1);

namespace Blazon\OAuth\Test\Command\Client;

use Blazon\OAuth\Command\Client\Create;
use Blazon\OAuth\Entity\ClientInterface;
use Blazon\OAuth\Exception\ClientExistsException;
use Blazon\OAuth\Exception\ClientNotFoundException;
use Blazon\OAuth\Repository\ClientRepositoryInterface;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;
use ReflectionMethod;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/** @covers \Blazon\OAuth\Command\Client\Create */
class CreateTest extends TestCase
{
    public function testConfigure()
    {
        $mockContainer = $this->createMock(ContainerInterface::class);
        $command = new Create($mockContainer);

        $name = $command->getName();
        $definition = $command->getDefinition();

        $this->assertEquals('oauth:client:create', $name);

        $arg = $definition->getArgument('name');
        $this->assertTrue($arg->isRequired());
    }

    public function testGetClientName()
    {
        $name = 'some name';

        $mockInput = $this->createMock(InputInterface::class);
        $mockClientRepo = $this->createMock(ClientRepositoryInterface::class);

        $command = $this->getMockBuilder(Create::class)
            ->onlyMethods(['getClientRepository'])
            ->disableOriginalConstructor()
            ->getMock();

        $command->expects($this->once())
            ->method('getClientRepository')
            ->willReturn($mockClientRepo);

        $mockInput->expects($this->once())
            ->method('getArgument')
            ->with($this->equalTo('name'))
            ->willReturn($name);

        $mockClientRepo->expects($this->once())
            ->method('findOneByName')
            ->with($this->equalTo($name))
            ->willThrowException(new ClientNotFoundException('testing'));

        $result = $command->getClientName($mockInput);
        $this->assertEquals($name, $result);
    }

    public function testGetClientNameAlreadyExists()
    {
        $this->expectException(ClientExistsException::class);

        $name = 'some name';

        $mockInput = $this->createMock(InputInterface::class);
        $mockClientRepo = $this->createMock(ClientRepositoryInterface::class);
        $mockClient = $this->createMock(ClientInterface::class);

        $command = $this->getMockBuilder(Create::class)
            ->onlyMethods(['getClientRepository'])
            ->disableOriginalConstructor()
            ->getMock();

        $command->expects($this->once())
            ->method('getClientRepository')
            ->willReturn($mockClientRepo);

        $mockInput->expects($this->once())
            ->method('getArgument')
            ->with($this->equalTo('name'))
            ->willReturn($name);

        $mockClientRepo->expects($this->once())
            ->method('findOneByName')
            ->with($this->equalTo($name))
            ->willReturn($mockClient);

        $command->getClientName($mockInput);
    }

    public function testExecute()
    {
        $name = 'some client name';
        $secret = 'some secret.';
        $redirect = 'http://somewhere.com';
        $grants = ['some grant'];
        $scopes = ['some scope'];

        $mockInput = $this->createMock(InputInterface::class);
        $mockOutput = $this->createMock(OutputInterface::class);
        $mockClientRepo = $this->createMock(ClientRepositoryInterface::class);

        $command = $this->getMockBuilder(Create::class)
            ->onlyMethods([
                'getClientName',
                'getSecret',
                'getRedirectUrl',
                'getGrants',
                'getScopes',
                'getClientRepository'
            ])->disableOriginalConstructor()
            ->getMock();

        $command->expects($this->once())
            ->method('getClientName')
            ->with($this->equalTo($mockInput))
            ->willReturn($name);

        $command->expects($this->once())
            ->method('getSecret')
            ->with(
                $this->equalTo($mockInput),
                $this->equalTo($mockOutput),
                $this->equalTo('client secret')
            )->willReturn($secret);

        $command->expects($this->once())
            ->method('getRedirectUrl')
            ->with(
                $this->equalTo($mockInput),
                $this->equalTo($mockOutput)
            )->willReturn($redirect);

        $command->expects($this->once())
            ->method('getGrants')
            ->with(
                $this->equalTo($mockInput),
                $this->equalTo($mockOutput)
            )->willReturn($grants);

        $command->expects($this->once())
            ->method('getScopes')
            ->with(
                $this->equalTo($mockInput),
                $this->equalTo($mockOutput)
            )->willReturn($scopes);

        $command->expects($this->once())
            ->method('getClientRepository')
            ->willReturn($mockClientRepo);

        $mockClientRepo->expects($this->once())
            ->method('createNewClient')
            ->with(
                $this->equalTo($name),
                $this->equalTo($secret),
                $this->equalTo($redirect),
                $this->equalTo($grants),
                $this->equalTo($scopes)
            );

        $mockOutput->expects($this->once())
            ->method('writeln')
            ->with($this->stringContains('created successfully'));

        $method = new ReflectionMethod($command, 'execute');
        $method->setAccessible(true);

        $result = $method->invokeArgs($command, [$mockInput, $mockOutput]);

        $this->assertEquals(0, $result);
    }
}
