<?php

declare(strict_types=1);

namespace Blazon\OAuth\Test\Command\Client;

use Blazon\OAuth\Command\Client\Grants;
use Blazon\OAuth\Command\Client\GrantsFactory;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;

/** @covers \Blazon\OAuth\Command\Client\GrantsFactory */
class GrantsFactoryTest extends TestCase
{
    public function testInvoke()
    {
        $container = $this->createMock(ContainerInterface::class);
        $factory = new GrantsFactory();

        $result = $factory($container);
        $this->assertInstanceOf(Grants::class, $result);
    }
}
