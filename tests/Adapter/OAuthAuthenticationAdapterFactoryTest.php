<?php

declare(strict_types=1);

namespace Blazon\OAuth\Test\Adapter;

use Blazon\OAuth\Adapter\OAuthAuthenticationAdapter;
use Blazon\OAuth\Adapter\OAuthAuthenticationAdapterFactory;
use Blazon\OAuth\Entity\AccessToken;
use Blazon\OAuth\Repository\AccessTokenRepository;
use Doctrine\ORM\EntityManagerInterface;
use League\OAuth2\Server\ResourceServer;
use Mezzio\ProblemDetails\ProblemDetailsResponseFactory;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;

/**
 * @covers \Blazon\OAuth\Adapter\OAuthAuthenticationAdapterFactory
 * @SuppressWarnings(PHPMD)
 */
class OAuthAuthenticationAdapterFactoryTest extends TestCase
{
    public function testInvoke()
    {
        $mockContainer = $this->createMock(ContainerInterface::class);
        $mockEntityManager = $this->createMock(EntityManagerInterface::class);
        $mockRepo = $this->createMock(AccessTokenRepository::class);
        $mockServer = $this->createMock(ResourceServer::class);
        $mockProblemFactory = $this->createMock(ProblemDetailsResponseFactory::class);

        $map = [
            ['Blazon\OAuth\Doctrine\EntityManager', $mockEntityManager],
            [ResourceServer::class, $mockServer],
            [ProblemDetailsResponseFactory::class, $mockProblemFactory],
        ];

        $mockContainer->expects($this->any())
            ->method('get')
            ->willReturnMap($map);

        $mockEntityManager->expects($this->once())
            ->method('getRepository')
            ->with($this->equalTo(AccessToken::class))
            ->willReturn($mockRepo);

        $factory = new OAuthAuthenticationAdapterFactory();
        $result = $factory($mockContainer);

        $this->assertInstanceOf(OAuthAuthenticationAdapter::class, $result);
    }
}
