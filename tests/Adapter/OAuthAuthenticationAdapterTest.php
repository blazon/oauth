<?php

declare(strict_types=1);

namespace Blazon\OAuth\Test\Adapter;

use Blazon\OAuth\Adapter\OAuthAuthenticationAdapter;
use Blazon\OAuth\Entity\AccessToken;
use Blazon\OAuth\Entity\User;
use Blazon\OAuth\Exception\AccessTokenNotFoundException;
use Blazon\OAuth\Repository\AccessTokenRepository;
use League\OAuth2\Server\Exception\OAuthServerException;
use League\OAuth2\Server\ResourceServer;
use Mezzio\ProblemDetails\ProblemDetailsResponseFactory;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @covers \Blazon\OAuth\Adapter\OAuthAuthenticationAdapter
 * @SuppressWarnings(PHPMD)
 */
class OAuthAuthenticationAdapterTest extends TestCase
{
    protected OAuthAuthenticationAdapter $adapter;
    protected ResourceServer | MockObject $mockServer;
    protected AccessTokenRepository | MockObject $mockRepository;
    protected ServerRequestInterface | MockObject $mockRequest;
    protected ResponseInterface | MockObject $mockResponse;
    protected AccessToken | MockObject $mockAccessToken;
    protected User | MockObject $mockUser;
    protected ProblemDetailsResponseFactory | MockObject $mockProblemDetailsResponseFactory;

    protected function setUp(): void
    {
        $this->mockServer = $this->getMockBuilder(ResourceServer::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->mockResponse = $this->createMock(ResponseInterface::class);

        $this->mockRepository = $this->getMockBuilder(AccessTokenRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->mockRequest = $this->createMock(ServerRequestInterface::class);

        $this->mockAccessToken = $this->getMockBuilder(AccessToken::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->mockUser = $this->getMockBuilder(User::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->mockProblemDetailsResponseFactory = $this->createMock(ProblemDetailsResponseFactory::class);

        $this->adapter = new OAuthAuthenticationAdapter(
            $this->mockServer,
            $this->mockRepository,
            $this->mockProblemDetailsResponseFactory
        );

        $this->assertInstanceOf(OAuthAuthenticationAdapter::class, $this->adapter);
    }

    public function testConstruct()
    {
    }

    public function testAuthenticate()
    {
        $token = 'some-token';

        $this->mockServer->expects($this->once())
            ->method('validateAuthenticatedRequest')
            ->with($this->equalTo($this->mockRequest))
            ->willReturn($this->mockRequest);

        $this->mockRequest->expects($this->once())
            ->method('getAttribute')
            ->with($this->equalTo('oauth_access_token_id'))
            ->willReturn($token);

        $this->mockRepository->expects($this->once())
            ->method('findAccessTokenByToken')
            ->with($this->equalTo($token))
            ->willReturn($this->mockAccessToken);

        $this->mockAccessToken->expects($this->once())
            ->method('getUser')
            ->willReturn($this->mockUser);

        $result = $this->adapter->authenticate($this->mockRequest);

        $this->assertEquals($this->mockUser, $result);
    }

    public function testAuthenticateMissingUser()
    {
        $token = 'some-token';

        $this->mockServer->expects($this->once())
            ->method('validateAuthenticatedRequest')
            ->with($this->equalTo($this->mockRequest))
            ->willReturn($this->mockRequest);

        $this->mockRequest->expects($this->once())
            ->method('getAttribute')
            ->with($this->equalTo('oauth_access_token_id'))
            ->willReturn($token);

        $this->mockRepository->expects($this->once())
            ->method('findAccessTokenByToken')
            ->with($this->equalTo($token))
            ->willReturn($this->mockAccessToken);

        $this->mockAccessToken->expects($this->once())
            ->method('getUser')
            ->willReturn(null);

        $result = $this->adapter->authenticate($this->mockRequest);

        $this->assertNull($result);
    }

    public function testAuthenticateAccessTokenIdNotFound()
    {
        $token = 'some-token';

        $this->mockServer->expects($this->once())
            ->method('validateAuthenticatedRequest')
            ->with($this->equalTo($this->mockRequest))
            ->willReturn($this->mockRequest);

        $this->mockRequest->expects($this->once())
            ->method('getAttribute')
            ->with($this->equalTo('oauth_access_token_id'))
            ->willReturn($token);

        $this->mockRepository->expects($this->once())
            ->method('findAccessTokenByToken')
            ->with($this->equalTo($token))
            ->willThrowException(new AccessTokenNotFoundException('testing'));

        $this->mockAccessToken->expects($this->never())
            ->method('getUser');

        $result = $this->adapter->authenticate($this->mockRequest);

        $this->assertNull($result);
    }

    public function testAuthenticateAccessTokenIdNotFoundInRequest()
    {
        $this->mockServer->expects($this->once())
            ->method('validateAuthenticatedRequest')
            ->with($this->equalTo($this->mockRequest))
            ->willReturn($this->mockRequest);

        $this->mockRequest->expects($this->once())
            ->method('getAttribute')
            ->with($this->equalTo('oauth_access_token_id'))
            ->willReturn(null);

        $this->mockRepository->expects($this->never())
            ->method('findAccessTokenByToken');

        $this->mockAccessToken->expects($this->never())
            ->method('getUser');

        $result = $this->adapter->authenticate($this->mockRequest);

        $this->assertNull($result);
    }

    public function testAuthenticateDenied()
    {
        $this->mockServer->expects($this->once())
            ->method('validateAuthenticatedRequest')
            ->with($this->equalTo($this->mockRequest))
            ->willThrowException(
                new OAuthServerException(
                    'testing',
                    401,
                    'error'
                )
            );

        $this->mockRequest->expects($this->never())
            ->method('getAttribute');

        $this->mockRepository->expects($this->never())
            ->method('findAccessTokenByToken');

        $this->mockAccessToken->expects($this->never())
            ->method('getUser');

        $result = $this->adapter->authenticate($this->mockRequest);

        $this->assertNull($result);
    }

    public function testUnauthorizedResponse()
    {
        $this->mockProblemDetailsResponseFactory->expects($this->once())
            ->method('createResponse')
            ->with(
                $this->equalTo($this->mockRequest),
                $this->equalTo(401),
                $this->equalTo('Unauthorized'),
            )->willReturn($this->mockResponse);

        $result = $this->adapter->unauthorizedResponse($this->mockRequest);

        $this->assertEquals($this->mockResponse, $result);
    }
}
