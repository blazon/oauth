<?php

namespace Blazon\OAuth\Test\Entity;

use Blazon\OAuth\Entity\AccessToken;
use Blazon\OAuth\Entity\AuthCode;
use Blazon\OAuth\Entity\Client;
use Doctrine\Common\Collections\ArrayCollection;
use League\OAuth2\Server\Entities\ScopeEntityInterface;
use PHPUnit\Framework\TestCase;

/** @SuppressWarnings(PHPMD) */
class ClientTest extends TestCase
{

    private $client;

    protected function setUp(): void
    {
        $this->client = new Client();
    }

    public function testSetGetIdentifier()
    {
        $identifier = 16;
        $this->client->setId($identifier);
        $result = $this->client->getIdentifier();
        $this->assertEquals($identifier, $result);
    }

    public function testSetGetGrants()
    {
        $grants = ['*', 'login', 'register'];
        $this->client->setGrants($grants);
        $result = $this->client->getGrants();
        $this->assertEquals($grants, $result);
    }

    public function testAddAuthCode()
    {
        $authCode = $this->getMockBuilder(AuthCode::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->client->addAuthCode($authCode);
        $result = $this->client->getAuthCodes();
        $this->assertEquals([$authCode], $result);
    }

    public function testSetGetId()
    {
        $id = 16;
        $this->client->setId($id);
        $result = $this->client->getId();
        $this->assertEquals($id, $result);
    }

    public function testSetGetRedirectUrl()
    {
        $url = 'http://google.com';
        $this->client->setRedirectUrl($url);
        $result = $this->client->getRedirectUrl();
        $this->assertEquals($url, $result);
    }

    public function testSetGetConfidential()
    {
        $confidential = true;
        $this->client->setConfidential($confidential);
        $result = $this->client->isConfidential();
        $this->assertEquals($confidential, $result);
        $this->assertIsBool($result);
    }

    public function testSetGetName()
    {
        $name = 'client-name';
        $this->client->setName($name);
        $result = $this->client->getName();
        $this->assertEquals($name, $result);
    }

    public function testAddAccessToken()
    {
        $accessToken = $this->getMockBuilder(AccessToken::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->client->addAccessToken($accessToken);
        $result = $this->client->getAccessTokens();
        $this->assertEquals([$accessToken], $result);
    }

    public function testSetGetSecret()
    {
        $secret = 'client-secret';
        $this->client->setSecret($secret);
        $result = $this->client->getSecret();
        $this->assertEquals($secret, $result);
    }

    public function testSetGetAccessTokens()
    {
        $accessToken = $this->getMockBuilder(AccessToken::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->client->setAccessTokens([$accessToken]);
        $result = $this->client->getAccessTokens();
        $this->assertEquals([$accessToken], $result);
    }

    public function testSetGetAuthCodes()
    {
        $authCode = $this->getMockBuilder(AuthCode::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->client->setAuthCodes([$authCode]);
        $result = $this->client->getAuthCodes();
        $this->assertEquals([$authCode], $result);
    }

    public function testAddScope()
    {
        $scope = $this->getMockBuilder(ScopeEntityInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->client->addScope($scope);
        $expectedScopes = [$scope];
        $result = $this->client->getScopes();

        $this->assertCount(1, $result);
        $this->assertEquals($expectedScopes, $result);
    }

    public function testSetScopes()
    {
        $scope = $this->getMockBuilder(ScopeEntityInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $scopes = [$scope];
        $this->client->setScopes($scopes);
        $expectedScopes = [$scope];
        $result = $this->client->getScopes();

        $this->assertCount(1, $result);
        $this->assertEquals($expectedScopes, $result);
    }
}
