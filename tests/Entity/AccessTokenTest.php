<?php

namespace Blazon\OAuth\Test\Entity;

use Blazon\OAuth\Entity\AccessToken;
use Blazon\OAuth\Entity\RefreshToken;
use Blazon\OAuth\Entity\UserInterface;
use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Entities\ScopeEntityInterface;
use PHPUnit\Framework\TestCase;

/**  */
class AccessTokenTest extends TestCase
{
    private $accessToken;

    protected function setUp(): void
    {
        $this->accessToken = new AccessToken();
    }

    public function testSetGetScope()
    {
        $scope = $this->getMockBuilder(ScopeEntityInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $scopes = [$scope];
        $this->accessToken->setScope($scopes);
        $result = $this->accessToken->getScopes();

        $this->assertCount(1, $result);
        $this->assertEquals($scopes, $result);
    }

    public function testSetGetUser()
    {
        $user = $this->getMockBuilder(UserInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->accessToken->setUser($user);
        $expectedUser = $this->accessToken->getUser();
        $this->assertEquals($expectedUser, $user);
    }

    public function testSetGetRefreshToken()
    {
        $refreshToken = $this->getMockBuilder(RefreshToken::class)
            ->enableArgumentCloning()
            ->disableOriginalConstructor()
            ->onlyMethods(['setAccessToken'])
            ->getMock();

        $this->accessToken->setRefreshToken($refreshToken);
        $result = $this->accessToken->getRefreshToken();

        $this->assertEquals($refreshToken, $result);
    }

    public function testSetGetExpires()
    {
        $expires = $this->getMockBuilder(\DateTimeImmutable::class)
            ->enableArgumentCloning()
            ->disableOriginalConstructor()
            ->getMock();

        $this->accessToken->setExpires($expires);
        $result = $this->accessToken->getExpires();
        $this->assertEquals($expires, $result);
    }

    public function testSetGetRevoked()
    {
        $revoked = true;
        $this->accessToken->setRevoked($revoked);
        $result = $this->accessToken->isRevoked();
        $this->assertEquals($revoked, $result);
    }

    public function testSetGetExpiryDateTime()
    {
        $expires = $this->getMockBuilder(\DateTimeImmutable::class)
            ->enableArgumentCloning()
            ->disableOriginalConstructor()
            ->getMock();

        $this->accessToken->setExpiryDateTime($expires);
        $result = $this->accessToken->getExpiryDateTime();
        $this->assertEquals($expires, $result);
    }

    public function testSetGetId()
    {
        $id = 16;
        $this->accessToken->setId($id);
        $result = $this->accessToken->getId();
        $this->assertEquals($id, $result);
    }

    public function testSetGetToken()
    {
        $token = 'some-token';
        $this->accessToken->setToken($token);
        $result = $this->accessToken->getToken();
        $this->assertEquals($token, $result);
    }

    public function testSetGetClient()
    {
        $client = $this->getMockBuilder(ClientEntityInterface::class)
            ->enableArgumentCloning()
            ->disableOriginalConstructor()
            ->getMock();

        $this->accessToken->setClient($client);
        $result = $this->accessToken->getClient();
        $this->assertEquals($client, $result);
    }

    public function testSetGetIdentifier()
    {
        $identifier = 'some-id';
        $this->accessToken->setIdentifier($identifier);
        $result = $this->accessToken->getIdentifier();
        $this->assertEquals($identifier, $result);
    }

    public function testSetGetUserIdentifier()
    {
        $user = $this->getMockBuilder(UserInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $user->expects($this->any())
            ->method('getIdentifier')
            ->willReturn('user-identifier');

        $this->accessToken->setUser($user);
        $result = $this->accessToken->getUserIdentifier();
        $this->assertEquals('user-identifier', $result);
    }

    public function testAddScope()
    {
        $scope = $this->getMockBuilder(ScopeEntityInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->accessToken->addScope($scope);
        $expectedScopes = [$scope];
        $result = $this->accessToken->getScopes();

        $this->assertCount(1, $result);
        $this->assertEquals($expectedScopes, $result);
    }
}
