<?php

namespace Blazon\OAuth\Test\Entity;

use Blazon\OAuth\Entity\AccessToken;
use Blazon\OAuth\Entity\AuthCode;
use Blazon\OAuth\Entity\Client;
use Blazon\OAuth\Entity\Scope;
use Blazon\OAuth\Entity\UserInterface;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;

class ScopeTest extends TestCase
{

    private $scope;

    protected function setUp(): void
    {
        $this->scope = new Scope();
    }

    public function testSetGetId()
    {
        $id = 16;
        $this->scope->setId($id);
        $result = $this->scope->getId();
        $this->assertEquals($id, $result);
    }

    public function testSetGetName()
    {
        $name = 'client-name';
        $this->scope->setName($name);
        $result = $this->scope->getName();
        $this->assertEquals($name, $result);
    }

    public function testGetIdentifier()
    {
        $name = 'client-name';
        $this->scope->setName($name);
        $result = $this->scope->getIdentifier();
        $this->assertEquals($name, $result);
    }

    public function testSetGetClients()
    {
        $client = $this->getMockBuilder(Client::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->scope->setClients([$client]);
        $result = $this->scope->getClients();
        $this->assertEquals(new ArrayCollection([$client]), $result);
    }

    public function testAddClient()
    {
        $client = $this->getMockBuilder(Client::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->scope->addClient($client);
        $result = $this->scope->getClients();
        $this->assertEquals(new ArrayCollection([$client]), $result);
    }

    public function testSetGetAuthCodes()
    {
        $authCode = $this->getMockBuilder(AuthCode::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->scope->setAuthCodes([$authCode]);
        $result = $this->scope->getAuthCodes();
        $this->assertEquals(new ArrayCollection([$authCode]), $result);
    }

    public function testAddAuthCode()
    {
        $authCode = $this->getMockBuilder(AuthCode::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->scope->addAuthCode($authCode);
        $result = $this->scope->getAuthCodes();
        $this->assertEquals(new ArrayCollection([$authCode]), $result);
    }

    public function testSetGetAccessTokens()
    {
        $accessToken = $this->getMockBuilder(AccessToken::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->scope->setAccessTokens([$accessToken]);
        $result = $this->scope->getAccessTokens();
        $this->assertEquals(new ArrayCollection([$accessToken]), $result);
    }

    public function testAddAccessToken()
    {
        $accessToken = $this->getMockBuilder(AccessToken::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->scope->addAccessToken($accessToken);
        $result = $this->scope->getAccessTokens();
        $this->assertEquals(new ArrayCollection([$accessToken]), $result);
    }

    public function testSetGetUsers()
    {
        $user = $this->getMockBuilder(UserInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->scope->setUsers([$user]);
        $result = $this->scope->getUsers();
        $this->assertEquals(new ArrayCollection([$user]), $result);
    }

    public function testAddUser()
    {
        $user = $this->getMockBuilder(UserInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->scope->addUser($user);
        $result = $this->scope->getUsers();
        $this->assertEquals(new ArrayCollection([$user]), $result);
    }

    public function testJsonSerialize()
    {
        $name = 'client-name';
        $this->scope->setName($name);
        $result = $this->scope->jsonSerialize();
        $this->assertEquals($name, $result);
    }
}
