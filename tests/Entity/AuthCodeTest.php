<?php

namespace Blazon\OAuth\Test\Entity;

use Blazon\OAuth\Entity\AuthCode;
use Blazon\OAuth\Entity\UserInterface;
use DateTimeImmutable;
use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Entities\ScopeEntityInterface;
use PHPUnit\Framework\TestCase;

/** @SuppressWarnings(PHPMD) */
class AuthCodeTest extends TestCase
{

    private $authCode;

    protected function setUp(): void
    {
        $this->authCode = new AuthCode();
    }

    public function testSetGetId()
    {
        $id = 16;
        $this->authCode->setId($id);
        $result = $this->authCode->getId();
        $this->assertEquals($id, $result);
    }

    public function testSetGetRedirectUrl()
    {
        $url = 'http://google.com';
        $this->authCode->setRedirectUrl($url);
        $result = $this->authCode->getRedirectUrl();
        $this->assertEquals($url, $result);
    }

    public function testSetGetRedirectUri()
    {
        $uri = 'http://google.com';
        $this->authCode->setRedirectUri($uri);
        $result = $this->authCode->getRedirectUri();
        $this->assertEquals($uri, $result);
    }

    public function testSetGetExpires()
    {
        $expires = $this->getMockBuilder(DateTimeImmutable::class)
            ->enableArgumentCloning()
            ->disableOriginalConstructor()
            ->getMock();

        $this->authCode->setExpires($expires);
        $result = $this->authCode->getExpires();
        $this->assertEquals($expires, $result);
    }

    public function testSetGetRevoked()
    {
        $revoked = true;
        $this->authCode->setRevoked($revoked);
        $result = $this->authCode->isRevoked();
        $this->assertEquals($revoked, $result);
    }

    public function testSetGetClient()
    {
        $client = $this->getMockBuilder(ClientEntityInterface::class)
            ->enableArgumentCloning()
            ->disableOriginalConstructor()
            ->getMock();

        $this->authCode->setClient($client);
        $result = $this->authCode->getClient();
        $this->assertEquals($client, $result);
    }

    public function testSetGetScope()
    {
        $scope = $this->getMockBuilder(ScopeEntityInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $scopes = [$scope];
        $this->authCode->setScopes($scopes);
        $expectedScopes = [$scope];
        $result = $this->authCode->getScopes();

        $this->assertIsArray($result);
        $this->assertCount(1, $result);
        $this->assertEquals($expectedScopes, $result);
    }

    public function testSetGetUserIdentifier()
    {
        $this->authCode->setUserIdentifier('user-identifier');
        $result = $this->authCode->getUserIdentifier();
        $this->assertEquals('user-identifier', $result);
    }

    public function testSetGetToken()
    {
        $token = 'some-token';
        $this->authCode->setToken($token);
        $result = $this->authCode->getToken();
        $this->assertEquals($token, $result);
    }

    public function testSetGetUser()
    {
        $user = $this->getMockBuilder(UserInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->authCode->setUser($user);
        $expectedUser = $this->authCode->getUser();
        $this->assertEquals($expectedUser, $user);
    }

    public function testSetGetIdentifier()
    {
        $identifier = 'some-id';
        $this->authCode->setIdentifier($identifier);
        $result = $this->authCode->getIdentifier();
        $this->assertEquals($identifier, $result);
    }

    public function testAddScope()
    {
        $scope = $this->getMockBuilder(ScopeEntityInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->authCode->addScope($scope);
        $expectedScopes = [$scope];
        $result = $this->authCode->getScopes();

        $this->assertIsArray($result);
        $this->assertCount(1, $result);
        $this->assertEquals($expectedScopes, $result);
    }

    public function testSetGetExpiryDateTime()
    {
        $expires = $this->getMockBuilder(DateTimeImmutable::class)
            ->enableArgumentCloning()
            ->disableOriginalConstructor()
            ->getMock();

        $this->authCode->setExpiryDateTime($expires);
        $result = $this->authCode->getExpiryDateTime();
        $this->assertEquals($expires, $result);
    }
}
