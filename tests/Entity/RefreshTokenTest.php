<?php

namespace Blazon\OAuth\Test\Entity;

use Blazon\OAuth\Entity\AccessToken;
use Blazon\OAuth\Entity\RefreshToken;
use PHPUnit\Framework\TestCase;

class RefreshTokenTest extends TestCase
{

    private $refreshToken;

    protected function setUp(): void
    {
        $this->refreshToken = new RefreshToken();
    }

    public function testSetGetAccessToken()
    {
        $refreshToken = $this->getMockBuilder(AccessToken::class)
            ->enableArgumentCloning()
            ->disableOriginalConstructor()
            ->onlyMethods(['setRefreshToken'])
            ->getMock();

        $this->refreshToken->setAccessToken($refreshToken);
        $result = $this->refreshToken->getAccessToken();

        $this->assertEquals($refreshToken, $result);
    }

    public function testSetGetExpires()
    {
        $expires = $this->getMockBuilder(\DateTimeImmutable::class)
            ->enableArgumentCloning()
            ->disableOriginalConstructor()
            ->getMock();

        $this->refreshToken->setExpires($expires);
        $result = $this->refreshToken->getExpires();
        $this->assertEquals($expires, $result);
    }

    public function testSetGetRevoked()
    {
        $revoked = true;
        $this->refreshToken->setRevoked($revoked);
        $result = $this->refreshToken->isRevoked();
        $this->assertEquals($revoked, $result);
    }

    public function testSetGetExpiryDateTime()
    {
        $expires = $this->getMockBuilder(\DateTimeImmutable::class)
            ->enableArgumentCloning()
            ->disableOriginalConstructor()
            ->getMock();

        $this->refreshToken->setExpiryDateTime($expires);
        $result = $this->refreshToken->getExpiryDateTime();
        $this->assertEquals($expires, $result);
    }

    public function testSetGetId()
    {
        $id = 16;
        $this->refreshToken->setId($id);
        $result = $this->refreshToken->getId();
        $this->assertEquals($id, $result);
    }

    public function testSetGetToken()
    {
        $token = 'some-token';
        $this->refreshToken->setToken($token);
        $result = $this->refreshToken->getToken();
        $this->assertEquals($token, $result);
    }


    public function testSetGetIdentifier()
    {
        $identifier = 'some-id';
        $this->refreshToken->setIdentifier($identifier);
        $result = $this->refreshToken->getIdentifier();
        $this->assertEquals($identifier, $result);
    }
}
