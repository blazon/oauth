<?php

namespace Blazon\OAuth\Test\Grant;

use DateInterval;
use Blazon\OAuth\Config\Config;
use Blazon\OAuth\Grant\RefreshTokenGrantFactory;
use Doctrine\ORM\EntityManager;
use League\OAuth2\Server\Grant\RefreshTokenGrant;
use League\OAuth2\Server\Repositories\RefreshTokenRepositoryInterface;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;

class RefreshTokenGrantFactoryTest extends TestCase
{
    private $grantFactory;

    protected function setUp(): void
    {
        $this->grantFactory = new RefreshTokenGrantFactory();
    }

    public function testInvoke()
    {
        $container = $this->getMockBuilder(ContainerInterface::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['get', 'has'])
            ->getMock();

        $entityManager = $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getRepository'])
            ->getMock();

        $refreshTokenRepository = $this->getMockBuilder(RefreshTokenRepositoryInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $entityManager->expects($this->once())
            ->method('getRepository')
            ->will($this->onConsecutiveCalls(
                $refreshTokenRepository
            ));

        $oauthConfig = $this->getMockBuilder(Config::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getRefreshTokenExpireInterval'])
            ->getMock();

        $refreshTokenTTL = $this->getMockBuilder(DateInterval::class)
            ->disableOriginalConstructor()
            ->getMock();

        $oauthConfig->expects($this->once())
            ->method('getRefreshTokenExpireInterval')
            ->willReturn(
                $refreshTokenTTL
            );

        $container->expects($this->exactly(2))
            ->method('get')
            ->will($this->onConsecutiveCalls(
                $entityManager,
                $oauthConfig
            ));

        $instance = $this->grantFactory->__invoke($container);
        $this->assertInstanceOf(RefreshTokenGrant::class, $instance);
    }
}
