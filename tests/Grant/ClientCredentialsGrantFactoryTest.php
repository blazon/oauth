<?php

namespace Blazon\OAuth\Test\Grant;

use Blazon\OAuth\Grant\AuthorizationCodeGrantFactory;
use Blazon\OAuth\Grant\ClientCredentialsGrantFactory;
use League\OAuth2\Server\Grant\ClientCredentialsGrant;
use PHPUnit\Framework\TestCase;

class ClientCredentialsGrantFactoryTest extends TestCase
{

    private $grantFactory;

    protected function setUp(): void
    {
        $this->grantFactory = new ClientCredentialsGrantFactory();
    }

    public function testInvoke()
    {
        $instance = $this->grantFactory->__invoke();
        $this->assertInstanceOf(ClientCredentialsGrant::class, $instance);
    }
}
