<?php

namespace Blazon\OAuth\Test\Grant;

use DateInterval;
use Blazon\OAuth\Config\Config;
use Blazon\OAuth\Grant\ImplicitGrantFactory;
use Doctrine\ORM\EntityManager;
use League\OAuth2\Server\Grant\ImplicitGrant;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;

class ImplicitGrantFactoryTest extends TestCase
{

    private $grantFactory;

    protected function setUp(): void
    {
        $this->grantFactory = new ImplicitGrantFactory();
    }

    public function testInvoke()
    {
        $container = $this->getMockBuilder(ContainerInterface::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['get', 'has'])
            ->getMock();

        $config = $this->getMockBuilder(Config::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getAccessTokenExpireInterval'])
            ->getMock();

        $dateInterval = $this->getMockBuilder(DateInterval::class)
            ->disableOriginalConstructor()
            ->getMock();

        $config->expects($this->once())
            ->method('getAccessTokenExpireInterval')
            ->willReturn($dateInterval);

        $container->expects($this->once())
            ->method('get')
            ->willReturn($config);

        $instance = $this->grantFactory->__invoke($container);
        $this->assertInstanceOf(ImplicitGrant::class, $instance);
    }
}
